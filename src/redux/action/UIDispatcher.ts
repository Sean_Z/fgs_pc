import store from '../store'
import {UIAction} from '../action/enum/UIAction'
import {MessageConfigIFS} from '@store/reducers/UIReducer'

export class UIDispatcher {
	public static setSnackMessage = (config: MessageConfigIFS) => {
		store.dispatch({
			type: UIAction.setSnackBarMessage,
			payload: config
		})
		setTimeout(() => {
			config.open = false
			store.dispatch({type: UIAction.setSnackBarMessage, payload: config})
		}, 2000)
	}
	public static closeLoading(): void {
		store.dispatch({type: UIAction.SET_LOADING, payload: false})
	}
}
