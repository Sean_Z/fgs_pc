import {combineReducers} from 'redux'
import {userReducerSlice} from './/reducers/UserReducer'
import {uiReducerSlice} from './/reducers/UIReducer'
import {fmsReducerSlice} from './/reducers/FmsReducer'
import {osmMapReducerSlice} from '../ui/widget/crew/onlinemap/assistant/OsmMapReducer'

const reducerMap = {
    userReducer: userReducerSlice.reducer,
    uiReducer: uiReducerSlice.reducer,
    fmsReducer: fmsReducerSlice.reducer,
    osmReducer: osmMapReducerSlice.reducer
}
export default combineReducers(reducerMap)