import {persistReducer, persistStore} from 'redux-persist'
import localStorage from 'redux-persist/es/storage'
import reducers from './reducer'
import {PersistConfig} from 'redux-persist/es/types'
import {configureStore} from '@reduxjs/toolkit'
import {TypedUseSelectorHook, useSelector} from 'react-redux'
import reducer from './/reducer'

const storageConfig: PersistConfig<any> = {
	key: 'root',
	storage: localStorage,
	whitelist: ['userReducer']
}
const myPersistReducer = persistReducer(storageConfig, reducers)
const store = configureStore({
	reducer: myPersistReducer,
	middleware: (getDefaultMiddleware) => getDefaultMiddleware({serializableCheck: false}),
})
type RootState = ReturnType<typeof reducer>
export const persist = persistStore(store)
export default store
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector