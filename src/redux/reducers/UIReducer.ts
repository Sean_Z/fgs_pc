import {createSlice, PayloadAction} from '@reduxjs/toolkit'

/**
 * @author Sean_Zhang
 * @date 2021/2/24 2:59 pm
 * @description 消息配置类型
 * @param open 是否打开消息条
 * @param messageType 消息类型
 * @param content 消息内容
 */
export interface MessageConfigIFS {
    open: boolean,
    messageType?: 'success' | 'info' | 'warning' | 'error' | undefined,
    content?: string | undefined
}

const initialState = {
    isLoading: false,
    showSnackMessage: {open: false, type: 'success', content: ''} as MessageConfigIFS
}

export const uiReducerSlice = createSlice({
    name: 'uiReducer',
    initialState,
    reducers: {
        setLoading(state = initialState, action: PayloadAction<boolean>) {
            state.isLoading = action.payload
        },
        setSnackBarMessage(state = initialState, action: PayloadAction<MessageConfigIFS>) {
            state.showSnackMessage = action.payload
        },
        closeLoading(state = initialState, action: PayloadAction<false>) {
            state.isLoading = action.payload
        }
    }
})

export const uiActions = uiReducerSlice.actions