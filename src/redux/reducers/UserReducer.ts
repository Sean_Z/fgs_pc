import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import {initNullState} from '../reducers/FmsReducer'
import {InfiniteFlightUser} from '@http/entity/response/InfiniteFlightUserRes'

const initialState = {
    currentUsername: '',
    position: '',
    isShowAvatar: false,
    email: '',
    userCurrentPosition: '',
    infiniteFlightUserId: '',
    infiniteFlightUserName: '',
    user: initNullState as unknown as InfiniteFlightUser
}

export const userReducerSlice = createSlice({
    name: 'userReducer',
    initialState,
    reducers: {
        setUserName(state = initialState, action: PayloadAction<string>) {
            state.currentUsername = action.payload
        },
        setUserEmail(state = initialState, action: PayloadAction<string>) {
            state.email = action.payload
        },
        setPosition(state = initialState, action: PayloadAction<string>) {
            state.position = action.payload
        },
        setIsShowAvatar(state = initialState, action: PayloadAction<boolean>) {
            state.isShowAvatar = action.payload
        },
        setCurrentPosition(state = initialState, action: PayloadAction<string>) {
            state.userCurrentPosition = action.payload
        },
        setUser(state = initialState, action: PayloadAction<InfiniteFlightUser>) {
            state.user = action.payload
        },
        setInfiniteFlightUserId(state = initialState, action: PayloadAction<string>) {
            state.infiniteFlightUserId = action.payload
        },
        setInfiniteFlightUserName(state = initialState, action: PayloadAction<string>) {
            state.infiniteFlightUserName = action.payload
        }
    }
})

export const userAction = userReducerSlice.actions