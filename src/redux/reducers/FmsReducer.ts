import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import {FlightDetail} from '@http/entity/response/AllServerFlightRes'
import {InfiniteFlightLog} from '@http/entity/response/InfiniteFlightLogsRes'
import {FetchOnlinePlanRes} from '@http/entity/response/FetchOnlinePlanRes'
import {AirwayLineRes} from '@http/entity/response/AirwayLineRes'

export const initNullState: unknown = undefined
const initialState = {
    currentFlightRoute: initNullState as FetchOnlinePlanRes,
    currentFlightDetail: initNullState as FlightDetail,
    currentAirway: [] as AirwayLineRes[],
    lastFlightLog: initNullState as InfiniteFlightLog,
    allFlightLogs: [] as InfiniteFlightLog[],
}

export const fmsReducerSlice = createSlice({
    name: 'fmsReducer',
    initialState,
    reducers: {
        setFlightRoute(state = initialState, action: PayloadAction<FetchOnlinePlanRes>) {
            state.currentFlightRoute = action.payload
        },
        setCurrentFlight(state = initialState, action: PayloadAction<FlightDetail>) {
            state.currentFlightDetail = action.payload
        },
        setCurrentAirway(state = initialState, action: PayloadAction<AirwayLineRes[]>) {
            state.currentAirway = action.payload
        },
        setLastFlightLog(state = initialState, action: PayloadAction<InfiniteFlightLog>) {
            state.lastFlightLog = action.payload
        },
        setAllFlightLogs(state = initialState, action: PayloadAction<InfiniteFlightLog[]>) {
            state.allFlightLogs = action.payload
        },
    }
})

export const fmsActions = fmsReducerSlice.actions