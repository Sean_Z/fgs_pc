import backgroundImg from '@assets/img/background-img/application-bar.jpeg'
import {styled} from '@material-ui/core'

export const UIAppBar = styled('div')({
	width: '100%',
	height: '200px',
	backgroundImage: `url(${backgroundImg})`,
	backgroundSize: 'cover',
	background: 'no-repeat center',
	overflow: 'hidden',
	marginTop: '4rem',
	boxShadow: '1px 3px 5px #696969',
})