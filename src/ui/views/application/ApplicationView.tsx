import React from 'react'
import NaviBar from '../../widget/nav-bar/NaviBar'
import Grid from '@material-ui/core/Grid'
import Requirement from '../../widget/application/Requirement'
import ApplicationForm from '../../widget/application/ApplicationForm'
import Foot from '../../widget/application/Foot'
import {UIAppBar} from './ApplicationPageStyledComponent'

/**
 * @method 此组件为申请加入FGS页面
 * @author Sean_Zhang
 */
export default function ApplicationView (): JSX.Element {

	return <Grid container>
		<NaviBar/>
		<UIAppBar />
		<Requirement/>
		<ApplicationForm/>
		<Foot/>
	</Grid>
}
