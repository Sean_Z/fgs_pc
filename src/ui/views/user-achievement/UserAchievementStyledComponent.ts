import {styled} from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
import {Constant} from '@assets/Constant'
import EmojiEventsIcon from '@mui/icons-material/EmojiEvents'

export const MainContainer = styled('div')({
    padding: 16
})

export const MainTitleView = styled(Typography)({
    fontWeight: 'bolder',
    color: Constant.Color.themeColor,
    display: 'flex',
    alignItems: 'center',
})

export const EmojiEventsIconView = styled(EmojiEventsIcon)({
    marginLeft: '1rem',
    color: Constant.Color.orange
})

export const FlightFrequencyCharts = styled('div')({
    height: 400,
    width: '100%'
})