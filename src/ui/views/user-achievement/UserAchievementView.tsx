import React, {useEffect} from 'react'
import Grid from '@material-ui/core/Grid'
import {
    EmojiEventsIconView,
    FlightFrequencyCharts,
    MainContainer,
    MainTitleView
} from './UserAchievementStyledComponent'
import CardContainer from '../../widget/public/CardContainer'
import {Tools} from '@core/Tools'
import {EChartsOption} from 'echarts'
import {useAppSelector} from '@store/store'
import {FgsService} from '@http/FgsService'
import {useDispatch} from 'react-redux'
import {fmsActions} from '@store/reducers/FmsReducer'
import {flightFrequencyConfig} from '../../widget/crew/dashboard/user-achievement/FlightFrequencyConfig'
import {InfiniteFlightLog, InfiniteFlightLogsRes} from '@http/entity/response/InfiniteFlightLogsRes'

export default function UserAchievementView(): JSX.Element {
    const userName = useAppSelector(state => state.userReducer.infiniteFlightUserName)
    const dispatch = useDispatch()
    const getLogs = async (): Promise<InfiniteFlightLog[]> => {
        const requestList: Promise<InfiniteFlightLogsRes>[] = []
        for (let i = 1; i < 5; i++) {
            const body = {userName, page: i}
            const request = FgsService.infiniteFlightLogs(body)
            requestList.push(request)
        }
        const logListRes = await Promise.all(requestList)
        const logList: InfiniteFlightLog[] = []
        logListRes.forEach(element => logList.push(...element?.data))
        dispatch(fmsActions.setAllFlightLogs([...logList]))
        return [...logList]
    }

    const groupByFlightLogs = (logs: InfiniteFlightLog[]): EChartsOption => {
        const flightLogMap: Map<string, InfiniteFlightLog[]> = new Map()
        const flightLogsGroupByDate: {logs: InfiniteFlightLog[], time: string}[] = []
        const timeList: string[] = []
        const landingList: number[] = []
        const flightList: number[] = []
        for (const log of logs) {
            const key = log.created.slice(0, 10)
            flightLogMap.set(key, logs.filter(_element => _element.created.slice(0, 10) === key))
        }
        flightLogMap.forEach((element, key) => flightLogsGroupByDate.push({logs: element, time: key}))
        for (let element of flightLogsGroupByDate) {
            const landings = element.logs.reduce((total, cur) => total + Number(cur.landingCount), 0)
            const flights = element.logs.reduce((total) => total + 1, 0)
            timeList.push(element.time)
            landingList.push(landings)
            flightList.push(flights)
        }
        return flightFrequencyConfig({timeList, flightList, landingList})
    }

    useEffect(() => {
        getLogs()
            .then((res) => groupByFlightLogs(res))
            .then((echartsOption) => Tools.buildEcharts('temp', echartsOption))
    }, [])

    return <>
        <MainContainer>
            <MainTitleView variant={'h3'}>
                User Achievement
                <EmojiEventsIconView sx={{fontSize: 100}}/>
            </MainTitleView>
            <Grid container>
                <Grid item xs={6}>
                    <CardContainer>
                        <FlightFrequencyCharts id={'temp'}/>
                    </CardContainer>
                </Grid>
            </Grid>
        </MainContainer>
    </>
}