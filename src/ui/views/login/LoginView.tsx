import React, {useEffect, useState} from 'react'
import CssBaseline from '@material-ui/core/CssBaseline'
import TextField from '@material-ui/core/TextField'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Checkbox from '@material-ui/core/Checkbox'
import Link from '@material-ui/core/Link'
import Paper from '@material-ui/core/Paper'
import Box from '@material-ui/core/Box'
import Grid from '@material-ui/core/Grid'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import Typography from '@material-ui/core/Typography'
import {
    BackgroundImageView,
    GithubIconView,
    LoginAvatar,
    LoginButton,
    LoginFormContainer,
    MainContainer,
    SubContainer
} from './LoginPageStyledComponent'
import {LoginType} from '../../../enum/Type'
import {Constant, Links} from '@assets/Constant'
import {Tools} from '@core/Tools'
import {FgsService} from '@http/FgsService'
import {useNavigate} from 'react-router'
import {useDispatch} from 'react-redux'
import {userAction} from '@store/reducers/UserReducer'
import {uiActions} from '@store/reducers/UIReducer'
import {LoginReq} from '@http/entity/request/LoginReq'
import {LoginRes} from '@http/entity/response/LoginRes'
import {useErrorSnackBar, useSuccessSnackBar} from '../../widget/public/UISnackbar'

/**
 * @method 此组件为登录页面
 * @Author Sean_Zhang
 */
const Copyright = () => {
    return (
        <Typography variant='body2' color='textSecondary' align='center'>
            {'Copyright © '}
            <Link color='inherit' href={Links.fgsLink}>
                Flight Global Studio
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    )
}
const loginData: LoginReq = {
    username: '',
    password: '',
    type: LoginType.ACCOUNT
}

export default function SignInSide(): JSX.Element {

    const history = useNavigate()
    const dispatch = useDispatch()
    const showSuccessSnackBar = useSuccessSnackBar()
    const showErrorSnackBar = useErrorSnackBar()

    const [isRememberMe, setIsRememberMe] = useState<boolean>(false)
    const [loginBtnConfig, setLoginBtnConfig] = useState<{ disable: boolean, btnName: string }>({
        disable: false,
        btnName: 'SIGN IN'
    })

    /**
     * 检查当前是否带有code，如果带了表示已经授权成功
     * 此时拿到code往服务端发送code，服务端做完剩下的校验
     */
    useEffect(() => {
        dispatch(uiActions.setLoading(false))
        setIsRememberMe(localStorage.getItem('isRememberMe') === '0')
        const code: string = Tools.getParam('code')
        if (code !== '') {
            setLoginBtnConfig({disable: true, btnName: 'Github login'})
            FgsService.githubLogin(code).then((res) => pubLogin(res))
        }
    }, [])

    /**
     * 登录结果公共方法
     * @param res 登录结果
     */
    const pubLogin = (res: LoginRes) => {
        const {nodeToken, accessToken, refreshToken, username, email, currentPosition, position, status} = res
        if (status === 0) {
            showSuccessSnackBar('Login successfully!')
            localStorage.setItem('nodeToken', nodeToken)
            localStorage.setItem('refreshToken', refreshToken)
            localStorage.setItem('accessToken', accessToken)
            dispatch(userAction.setUserName(username))
            dispatch(userAction.setIsShowAvatar(true))
            dispatch(userAction.setUserEmail(email))
            dispatch(userAction.setCurrentPosition(currentPosition))
            dispatch(userAction.setPosition(position))
            dispatch(userAction.setInfiniteFlightUserId(res.infiniteFlightUserId))
            dispatch(userAction.setInfiniteFlightUserName(res.infiniteFlightUserName))
            history('/crew')
        } else {
            setLoginBtnConfig({disable: false, btnName: 'SIGN IN'})
            showErrorSnackBar(res.message)
        }
    }

    const resetPassword = (): void => {
        uiActions.setSnackBarMessage({open: true, content: 'show snack'})
    }

    /**
     * github登录获取code
     */
    const githubLogin = (): void => {
        const userCodeUrl: string = Constant.GITHUB_USER.requestCodeUri
        window.location.href = `${userCodeUrl}client_id=${Constant.GITHUB_USER.clientId}`
    }

    /**
     * @method login
     * @description 登录
     * @return void
     */
    const login = async (): Promise<void> => {
        setLoginBtnConfig({disable: true, btnName: 'SIGN IN'})
        const loginInfo: LoginReq = {
            username: loginData.username,
            password: loginData.password,
            type: LoginType.ACCOUNT
        }
        const res = await FgsService.login(loginInfo)
        pubLogin(res)
    }

    const setRemember = (): void => {
        localStorage.setItem('isRememberMe', localStorage.getItem('isRememberMe') === '0' ? '1' : '0')
        setIsRememberMe(localStorage.getItem('isRememberMe') === '0')
    }

    // 回车触发登录操作
    onkeydown = (e: KeyboardEvent): void => {
        e.key === 'Enter' && login()
    }

    return <>
        <MainContainer container>
            <CssBaseline/>
            <BackgroundImageView item xs={false} sm={4} md={7}/>
            <Grid
                item
                xs={12}
                sm={8}
                md={5}
                component={Paper}
                elevation={6}
                square>
                <SubContainer>
                    <LoginAvatar>
                        <LockOutlinedIcon/>
                    </LoginAvatar>
                    <Typography component='h1' variant='h5'>
                        Sign in
                    </Typography>
                    <LoginFormContainer>
                        <TextField
                            variant='outlined'
                            margin='normal'
                            required
                            fullWidth
                            id='username'
                            label='Email Address'
                            name='username'
                            autoFocus
                            onInput={(event: React.ChangeEvent<HTMLInputElement>) => loginData.username = event.target.value}/>
                        <TextField
                            variant='outlined'
                            margin='normal'
                            required
                            fullWidth
                            name='password'
                            label='Password'
                            type='password'
                            id='password'
                            onInput={(event: React.ChangeEvent<HTMLInputElement>) => loginData.password = event.target.value}/>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    value='remember'
                                    color='primary'
                                    onChange={setRemember}
                                    checked={isRememberMe}/>}
                            label='Remember me'/>
                        <LoginButton
                            onClick={login}
                            fullWidth
                            variant='contained'
                            color='primary'
                            disabled={loginBtnConfig.disable}>
                            {loginBtnConfig.btnName}
                        </LoginButton>
                        <Grid container>
                            <Grid item xs>
                                <Link href='src/ui/views/login/LoginView#' variant='body2' onClick={resetPassword}>
                                    Forgot password?
                                </Link>
                            </Grid>
                        </Grid>
                        <Box mt={5} textAlign={'center'}>
                            <GithubIconView onClick={githubLogin}/>
                            <Copyright/>
                        </Box>
                    </LoginFormContainer>
                </SubContainer>
            </Grid>
        </MainContainer>
    </>
}
