import fgsLogo from '@assets/img/logo/fgsLogo.png'
import {styled} from '@material-ui/core'
import Grid from '@material-ui/core/Grid'
import Avatar from '@material-ui/core/Avatar'
import Button from '@material-ui/core/Button'
import GitHubIcon from '@material-ui/icons/GitHub'
import {theme} from '../../theme/Theme'

export const MainContainer = styled(Grid)({
	height: '100vh',
})

export const SubContainer = styled('div')({
	margin: theme.spacing (8, 4),
	display: 'flex',
	flexDirection: 'column',
	alignItems: 'center',
})

export const LoginAvatar = styled(Avatar)({
	margin: theme.spacing (1),
	backgroundColor: theme.palette.secondary.main,
	backgroundImage: `url(${fgsLogo})`,
})

export const BackgroundImageView = styled(Grid)({
	backgroundImage: 'url(https://api.dujin.org/bing/1920.php)',
	backgroundRepeat: 'no-repeat',
	backgroundColor: theme.palette.grey[50],
	backgroundSize: 'cover',
	backgroundPosition: 'center',
})

export const LoginFormContainer = styled('form')({
	width: '100%',
	marginTop: theme.spacing (1),
})

export const LoginButton = styled(Button)({
	margin: theme.spacing (3, 0, 2),
})

export const GithubIconView = styled(GitHubIcon)({
	cursor: 'pointer'
})