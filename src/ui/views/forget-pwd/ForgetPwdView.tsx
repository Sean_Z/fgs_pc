import React, {useState} from 'react'
import {Input, TextField} from '@material-ui/core'
import CardContent from '@material-ui/core/CardContent'
import {CardTitleTextView, MainContainer, ResetButton, ResetPwdCardView} from './ForgetPwdPageStyledComponent'
import RotateLeftIcon from '@material-ui/icons/RotateLeft'
import logo from '@assets/img/logo/logo.png'
import Avatar from '@material-ui/core/Avatar'
import Typography from '@material-ui/core/Typography'
import {FgsService} from '@http/FgsService'
import {useSuccessSnackBar} from '../../widget/public/UISnackbar'

export default function ForgetPwdView(): JSX.Element {
    const [isVerifyCodeInput, setIsVerifyCodeInput] = useState<boolean>(true)
    const [mail, setMail] = useState<string>('')
    const [verifyCode, setVerifyCode] = useState<string>('')
    const [newPassword, setNewPassword] = useState<string>('')
    const [verifyBtnName, setVerifyBtnName] = useState<string>('Get Code')
    const [btnStatus, setBtnStatus] = useState<boolean>(true)
    const [resetBtnStatus, setResetBtnStatus] = useState<boolean>(true)
    const [isPassedCheck, setIsPassedCheck] = useState<boolean>(false)
    const showSuccessSnackBar = useSuccessSnackBar()
    let _emailAddress: string = ''

    const countDown = (initNumber: number): void => {
        setBtnStatus(true)
        const timer = setInterval((): void => {
            initNumber -= 1
            setVerifyBtnName(`Resend in ${initNumber} sec`)
            if (initNumber === 0) {
                setVerifyBtnName(`Get Code`)
                setBtnStatus(false)
                clearInterval(timer)
            }
        }, 1000)
    }

    const setEmail = (emailAddress: string): void => {
        const reg = /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/
        if (reg.test(emailAddress)) {
            setMail(emailAddress)
            setIsPassedCheck(false)
            setBtnStatus(false)
            _emailAddress = emailAddress
        } else {
            setIsPassedCheck(true)
            setBtnStatus(true)
        }
    }

    const sendVerifyCode = async (emailAddress: string): Promise<void> => {
        countDown(10)
        const res = await FgsService.sendVerifyCode(emailAddress)
        const {status, message} = res
        if (status === 0) {
            showSuccessSnackBar(message)
            setIsVerifyCodeInput(false)
            setVerifyBtnName('Get code')
            setBtnStatus(false)
            setResetBtnStatus(false)
            setMail('')
        }
    }

    const toVerifyCode = async(): Promise<void> => {
        await FgsService.resetPwd({
            email: _emailAddress,
            'verifyCode': verifyCode,
            resetPwd: newPassword
        })
    }

    return <MainContainer>
        <ResetPwdCardView>
            <CardTitleTextView
                title={<Typography variant={'h6'}>Forget password</Typography>}
                avatar={<Avatar src={logo}/>}/>
            <CardContent>
                <form>
                    <TextField
                        error={isPassedCheck}
                        fullWidth={true}
                        required={true}
                        label={isVerifyCodeInput ? 'email' : 'New password'}
                        placeholder={'your email'}
                        onInput={
                            isVerifyCodeInput ? (event: React.ChangeEvent<HTMLInputElement>) => setEmail(event.target.value) :
                                (event: React.ChangeEvent<HTMLInputElement>) => setNewPassword(event.target.value)}/>
                    <br/>
                    <Input
                        disabled={btnStatus}
                        fullWidth={true}
                        required={true}
                        placeholder={'verify code'}
                        onInput={(event: React.ChangeEvent<HTMLInputElement>) => setVerifyCode(event.target.value)}
                        endAdornment={
                            <ResetButton
                                variant="contained"
                                color="primary"
                                size={'small'}
                                disableElevation
                                style={{width: '7rem'}}
                                onClick={isVerifyCodeInput ? () => sendVerifyCode(mail) : () => toVerifyCode()}
                                disabled={btnStatus}
                            >{verifyBtnName}</ResetButton>}
                    />
                    <br/>
                    <ResetButton
                        variant="contained"
                        color="primary"
                        onClick={() => toVerifyCode()}
                        endIcon={<RotateLeftIcon/>}
                        disabled={resetBtnStatus}>
                        Reset
                    </ResetButton>
                </form>
            </CardContent>
        </ResetPwdCardView>
    </MainContainer>
}