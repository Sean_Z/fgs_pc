import {Constant} from '@assets/Constant'
import {styled} from '@material-ui/core'
import Card from '@material-ui/core/Card'
import Button from '@material-ui/core/Button'
import CardHeader from '@material-ui/core/CardHeader'

export const MainContainer = styled('div')({
	height: '100%',
	width: '100%',
	textAlign: 'center',
	position: 'absolute',
	backgroundImage: 'url(https://api.dujin.org/bing/1920.php)',
	display: 'flex',
	justifyContent: 'center',
	alignItems: 'center'
})

export const ResetPwdCardView = styled(Card)({
	width: '400px',
	paddingTop: '50px',
	position: 'relative'
})

export const ResetButton = styled(Button)({
	background: Constant.Color.themeColor,
	color: 'white',
	position: 'relative',
	margin: '2rem 0 0 0'
})

export const CardTitleTextView = styled(CardHeader)({
	position: 'absolute',
	left: '50%',
	width: '220px',
	transform: 'translate(-50%, -50%)'
})