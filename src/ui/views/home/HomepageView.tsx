import React from 'react'
import NaviBar from '../../widget/nav-bar/NaviBar'
import MainInfo from '../../widget/homepage/MainInfo'
import {MainContainer, PopUpView, TopBlockArea} from './HomePageStyledComponent'

export default function HomepageView(): JSX.Element {
	return <>
		<TopBlockArea>
			<NaviBar/>
			<PopUpView />
		</TopBlockArea>
		<MainContainer>
			<MainInfo />
		</MainContainer>
	</>
}
