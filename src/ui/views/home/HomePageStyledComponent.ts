import {Constant} from '@assets/Constant'
import homepageBgiImg from '@assets/img/background-img/homepage.png'
import {styled} from '@material-ui/core'

export const MainContainer = styled('div')({
	width: '100%',
	position: 'absolute'
})

export const TopBlockArea = styled('div')({
	backgroundImage: `url(${homepageBgiImg})`,
	backgroundRepeat: 'noRepeat',
	backgroundPosition: 'center',
	backgroundSize: 'cover',
	position: 'absolute',
	overflow: 'scroll',
	width: '100%',
	height: '100%',
})

export const PopUpView = styled('div')({
	backgroundColor: Constant.Color.themeColor,
	opacity: '0.5',
	position: 'absolute',
	width: '100%',
	height: '100%',
})