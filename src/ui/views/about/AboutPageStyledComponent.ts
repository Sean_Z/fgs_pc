import {Constant} from '@assets/Constant'
import {styled} from '@material-ui/core'

export const MainContainer = styled('div')({
	textAlign: 'center',
	backgroundColor: Constant.Color.themeColor
})