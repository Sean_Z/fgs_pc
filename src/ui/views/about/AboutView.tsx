import React from 'react'
import NaviBar from '../../widget/nav-bar/NaviBar'
import MemberCard from '../../widget/about/MemberCard'
import Swiper from '../../widget/about/Swiper'
import Title from '../../widget/about/Title'
import Foot from '../../widget/application/Foot'
import {MainContainer} from './AboutPageStyledComponent'

/**
 * @method 此组件为成员信息页面
 * @author Sean_Zhang
 */
export default function AboutView (): JSX.Element {

	return <MainContainer>
		<NaviBar/>
		<Swiper/>
		<Title/>
		<MemberCard/>
		<Foot/>
	</MainContainer>
}

