import React, {lazy, Suspense} from 'react'
import {Route, Routes} from 'react-router-dom'
import {MainContainer} from './CrewPageStyledComponent'
import Backdrop from '@material-ui/core/Backdrop'
import CircularProgress from '@material-ui/core/CircularProgress'
import UserAchievementView from '../user-achievement/UserAchievementView'

const LeafletOnlineMap = lazy(() => import('../../widget/crew/onlinemap/LeafletOnlineMap'))
const DashBoard = lazy(() => import('../../widget/crew/dashboard/DashBoard'))
const Drawers = lazy(() => import('../../widget/crew/drawer/Drawer'))
const Tutorial = lazy(() => import('../../widget/crew/tutorial/Tutorial'))

export default function CrewView(props: any): JSX.Element {

    return <MainContainer>
        <Drawers window={props}/>
        <ViewGroup/>
    </MainContainer>
}

const ViewGroup = (): JSX.Element => {
    const BackMask = () =>
        <Backdrop style={{zIndex: 20000, color: 'white'}} open={true}>
            <CircularProgress style={{zIndex: 20000}} color='inherit'/>
        </Backdrop>
    return <Suspense fallback={<BackMask/>}>
        <Routes>
            <Route path='/status' element={<LeafletOnlineMap/>}/>
            <Route path='/achievement' element={<UserAchievementView/>}/>
            <Route path='/tutorial' element={<Tutorial/>}/>
            <Route path='/' element={<DashBoard/>}/>
        </Routes>
    </Suspense>
}