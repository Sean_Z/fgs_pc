import {Components, createTheme, ThemeOptions} from '@mui/material'
import {Colors} from './Colors'

const overrideButton: Components['MuiButton'] = {defaultProps: {variant: 'contained', color: 'primary'}}
const overrideMuiListItem = {styleOverrides: {root: {paddingLeft: 0, paddingRight: 0}}}
const overrideMuiCard = {defaultProps: {elevation: 4}, styleOverrides: {root: {borderRadius: 8, padding: '1rem'}}}
const overrideMuiTypography = {styleOverrides: {root: {color: Colors.primary}}}
const overrideMuiIcon = {styleOverrides: {root: {color: Colors.primary}}}
const overrideMuiListItemText = {styleOverrides: {root: {color: Colors.primary}}}

const options: ThemeOptions = {
	shape: {borderRadius: 4},
	components: {
		MuiButton: overrideButton,
		MuiListItem: overrideMuiListItem,
		MuiCard: overrideMuiCard,
		MuiTypography: overrideMuiTypography,
		MuiIcon: overrideMuiIcon,
		MuiListItemText: overrideMuiListItemText
	},
	palette: {
		primary: {
			main: Colors.primary,
			light: Colors.primary
		},
		secondary: {
			main: Colors.primary,
			light: Colors.primary
		},
	}
}
export const theme = createTheme(options)