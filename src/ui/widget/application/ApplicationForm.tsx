import React, {forwardRef, useEffect, useRef, useState} from 'react'
import Grid from '@material-ui/core/Grid'
import CardHeader from '@material-ui/core/CardHeader'
import CardContent from '@material-ui/core/CardContent'
import MenuItem from '@material-ui/core/MenuItem'
import Button from '@material-ui/core/Button'
import Icon from '@material-ui/core/Icon'
import DeleteIcon from '@material-ui/icons/Delete'
import {ActionButton, ButtonGroup, ButtonProgress, MainContainer, TextInput} from './ApplicationStyledComponent'
import {Constant} from '@assets/Constant'
import axios from 'axios'
import {FgsService} from '@http/FgsService'
import {ApplicantPositionRes} from '@http/entity/response/ApplicationPositionRes'
import {useErrorSnackBar, useSuccessSnackBar} from '../public/UISnackbar'

export interface AgesList {
    readonly value: string,
    readonly label: string
}
function ApplicationFormFC(): JSX.Element {
    const [flightTime, setFlightTime] = useState<number>(0)
    const [violations, setViolations] = useState<number>(0)
    const [landings, setLandings] = useState<number>(0)
    const [totalXP, setTotalXP] = useState<number>(0)
    const [currency, setCurrency] = useState<string>('')
    const [age, setAge] = useState<string>('18-20')
    const [loading, setLoading] = useState<boolean>(false)
    const [fetchBtnStatus, setFetchBtnStatus] = useState<boolean>(false)
    const displayNameInputRef = useRef<HTMLInputElement>(null)
    const emailInputRef = useRef<HTMLInputElement>(null)
    const joinedGroupInputRef = useRef<HTMLInputElement>(null)

    useEffect(() => setUserPosition(), [])
    const showSuccessSnackbar = useSuccessSnackBar()
    const showErrorSnackbar = useErrorSnackBar()

    /**
     * @method userPosition
     * @description 查询注册用户当前所在地
     * @returns {Promise<object | void>}
     */
    const setUserPosition = (): void => {
        axios.get('http://ip-api.com/json').then(response => {
            const {city, country} = response as unknown as ApplicantPositionRes
            setCurrency(`${city} of ${country}`)
        })
    }

    /**
     * @method fetchUserInfo
     * @description 查询申请人目前在线时所有的用户真实数据
     * @return void
     */
    const fetchUserInfo = async (): Promise<void> => {
        setLoading(true)
        setFetchBtnStatus(true)
        const displayName = displayNameInputRef.current?.value ?? ''
        if (displayName !== '') {
            const res = await FgsService.getUserInfo(displayName)
            setLoading(false)
            setFetchBtnStatus(false)
            const {violations, flightTime, landingCount, xp} = res
            setViolations(violations)
            setLandings(landingCount)
            setTotalXP(Number(xp))
            setFlightTime(flightTime)
            setLoading(false)
        }
    }

    /**
     * @method clearForm
     * @description 清空表单所有内容，除所在国家
     * @param ifClearJoined
     */
    const clearForm = (ifClearJoined: boolean): void => {
        setFlightTime(0)
        setViolations(0)
        setLandings(0)
        setTotalXP(0)
        setLoading(false)
        setFetchBtnStatus(false)
        emailInputRef.current!.value = ''
        if (ifClearJoined) joinedGroupInputRef.current!.value = ''
    }

    /**
     * @method inputDisplayNameAction
     * @description 监听用户名状态，若用户名为空则获取按钮状态置为    False，保留表单内容，反之清空表单内容，获取按钮置为True重新开始
     * @return void
     */
    const inputDisplayNameAction = (): void => {
        if (displayNameInputRef.current?.value ?? '' !== '') {
            setFetchBtnStatus(false)
        } else {
            setFetchBtnStatus(true)
            clearForm(false)
        }
    }

    /**
     * @method submitForm
     * @description 调用邮件接口发送申请表
     * @return void
     */
    const submitForm = async (): Promise<void> => {
        const submitData = {
            age,
            email: emailInputRef.current?.value ?? '',
            landings,
            violations,
            joinedGroup: joinedGroupInputRef.current?.value ?? '',
            country: currency,
            ifFlightTime: flightTime,
            username: displayNameInputRef.current?.value ?? ''
        }
        const {status, message} = await FgsService.submitApplication(submitData)
        status === 1 ? showErrorSnackbar(message) : showSuccessSnackbar(message)
    }
    return <>
        <MainContainer>
            <CardHeader title='ApplicationView Form'/>
            <CardContent>
                <form>
                    <Grid container spacing={3}>
                        <Grid item xs={4}>
                            <TextInput
                                id='name'
                                label='Display Name'
                                placeholder='Your Display Name'
                                variant='outlined'
                                inputRef={displayNameInputRef}
                                onInput={() => inputDisplayNameAction()}/>
                        </Grid>
                        <Grid item xs={4}>
                            <TextInput
                                label='Age'
                                select
                                placeholder='Your Age'
                                value={age}
                                onChange={(event: React.ChangeEvent<HTMLInputElement>) => setAge(event.target.value)}
                                variant='outlined'>
                                {Constant.AGES.map((option: AgesList) =>
                                    <MenuItem key={option.value} value={option.value}>
                                        {option.label}
                                    </MenuItem>
                                )}
                            </TextInput>
                        </Grid>
                        <Grid item xs={4}>
                            <TextInput
                                label='Country'
                                placeholder='Your country'
                                variant='outlined'
                                value={currency}
                                disabled={true}>
                            </TextInput>
                        </Grid>
                    </Grid>
                    <Grid container spacing={3}>
                        <Grid item xs={4}>
                            <TextInput
                                label='Flight Time'
                                type='number'
                                placeholder='Your Flight Time'
                                variant='outlined'
                                value={flightTime}
                                disabled={true}/>
                        </Grid>
                        <Grid item xs={4}>
                            <TextInput
                                label='Violation'
                                placeholder='Violation'
                                variant='outlined'
                                value={violations}
                                disabled={true}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextInput
                                label='landings'
                                type='number'
                                placeholder='Your landings'
                                variant='outlined'
                                value={landings}
                                disabled={true}/>
                        </Grid>
                    </Grid>
                    <Grid container spacing={3}>
                        <Grid item xs={4}>
                            <TextInput
                                label='XP'
                                placeholder='Your experience in IF'
                                variant='outlined'
                                value={totalXP}
                                disabled={true}/>
                        </Grid>
                        <Grid item xs={4}>
                            <TextInput
                                label='Email'
                                placeholder='email'
                                variant='outlined'
                                inputRef={emailInputRef}/>
                        </Grid>
                        <Grid item xs={4}>
                            <TextInput
                                label='Group you already joined'
                                placeholder='CFU'
                                variant='outlined'
                                inputRef={joinedGroupInputRef}/>
                        </Grid>
                    </Grid>
                    <ButtonGroup item xs={12}>
                        <ActionButton
                            variant='contained'
                            color='primary'
                            startIcon={<Icon>send</Icon>}
                            onClick={submitForm}>
                            Send
                        </ActionButton>
                        <ActionButton
                            variant='contained'
                            color='primary'
                            startIcon={<Icon>send</Icon>}
                            onClick={() => fetchUserInfo()}
                            disabled={fetchBtnStatus}>
                            fetch your status
                        </ActionButton>
                        {loading && <ButtonProgress size={24}/>}
                        <Button
                            variant='contained'
                            color='secondary'
                            startIcon={<DeleteIcon/>}
                            onClick={() => clearForm(false)}>
                            Clear
                        </Button>
                    </ButtonGroup>
                </form>
            </CardContent>
        </MainContainer>
    </>
}

const ApplicationForm = React.memo(forwardRef(() => <ApplicationFormFC/>))
export default ApplicationForm