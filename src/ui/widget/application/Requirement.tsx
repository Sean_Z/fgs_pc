import React from 'react'
import Typography from '@material-ui/core/Typography'
import {ArticleContainer, UIDivider} from './ApplicationStyledComponent'

/**
 * @method 此组件为application页面的招新要求介绍模块
 * @Author Sean_Zhang
 */

export default function Requirement (): JSX.Element {

	return (
		<ArticleContainer>
			<Typography variant='h4' component='h2' gutterBottom>
				How to Join Us ?
			</Typography>
			<UIDivider />
			<Typography variant='h6' component='h2' gutterBottom>
				Welcome to the Flight Global Studio Application Form! We are happy to see your interest in joining our
				team!
			</Typography>
			<Typography variant='h6' component='h2' gutterBottom>
				There are two steps in our recruitment process:
			</Typography>
			<Typography variant='body1' gutterBottom>
				1. Application Form <br/>
				2. Written Entrance Exam
			</Typography>
			<Typography variant='h6' component='h2' gutterBottom>
				Minimum requirements to apply are as follows:
			</Typography>
			<Typography variant='body1' gutterBottom>
				1. Be 18 years of age or older.<br/>
				2. Be Grade 3 or higher.<br/>
				3. Have a valid Infinite Flight Community (IFC) account.<br/>
				4. Have a valid Infinite Flight Pro Subscription.<br/>
				5. Have a minimum of 200 flight hours on Infinite Flight.<br/>
				6. Have a maximum of 20 violations and 2 reports/ghosts in 1 year.<br/>
				7. Have a minimum of 20 landings in the last 90 days.
			</Typography>
			<Typography variant='subtitle1' gutterBottom color={'initial'}>
				<div style={{color: 'red'}}>If you satisfy the above requirements, please enter the infinite flight at first ,then type your display name and click fetch your status button to<br/>
				complete the application form with the necessary
				information.</div> We will get in touch with you via email within 24 hours.
			</Typography>
		</ArticleContainer>
	)
}
