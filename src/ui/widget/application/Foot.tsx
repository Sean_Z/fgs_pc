import React from 'react'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import {
	CodeIconView,
	FootText,
	FootTextContainer,
	FootTextView,
	FootView,
	GroupWorkIconView,
	LoyaltyIconView,
	MonetizationOnIconView,
	UIDivider
} from './ApplicationStyledComponent'

/**
 * @method 此组件为网页底部模块
 * @Author Sean_Zhang
 */

export default function Foot (): JSX.Element {

	return (
		<>
			<FootView>
				<FootTextContainer container>
					<Grid item xs={4}>
						<Typography variant='h6' gutterBottom>
							<GroupWorkIconView />Cooperation Partner
						</Typography>
						<FootTextView variant='subtitle2' gutterBottom>
							CHINA FLIGHT UNION
						</FootTextView>
						<FootTextView variant='subtitle2' gutterBottom>
							FGS WEB Develop Studio
						</FootTextView>
						<FootTextView variant='subtitle2' gutterBottom>
							Hainan Airline
						</FootTextView>
					</Grid>
					<Grid item xs={4}>
						<Typography variant='h6' gutterBottom>
							<MonetizationOnIconView />
							Sponsor
						</Typography>
						<FootTextView variant='subtitle2' gutterBottom>
							HENG ZONG
						</FootTextView>
						<FootTextView variant='subtitle2' gutterBottom>
							CHAORAN YANG
						</FootTextView>
						<FootTextView variant='subtitle2' gutterBottom>
							HAO YOU
						</FootTextView>
						<FootTextView variant='subtitle2' gutterBottom>
							ANDY ZHENG
						</FootTextView>
					</Grid>
					<Grid item xs={4}>
						<Typography variant='h6' gutterBottom>
							<CodeIconView />
							Developer
						</Typography>
						<FootTextView variant='subtitle2' gutterBottom>
							Sean_Zhang
						</FootTextView>
						<FootTextView variant='subtitle2' gutterBottom>
							Edison Li
						</FootTextView>
						<FootTextView variant='subtitle2' gutterBottom>
							Annabelle Zhang
						</FootTextView>
						<FootTextView variant='subtitle2' gutterBottom>
							Max Chen
						</FootTextView>
					</Grid>
				</FootTextContainer>
				<UIDivider />
				<FootText>
					<LoyaltyIconView />
					MADE WITH
					FGS WEB DEVELOP STUDIO
				</FootText>
			</FootView>
		</>
	)
}
