import {green} from '@material-ui/core/colors'
import {Constant} from '@assets/Constant'
import {styled} from '@material-ui/core'
import Card from '@material-ui/core/Card'
import TextField from '@material-ui/core/TextField'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'
import GroupWorkIcon from '@material-ui/icons/GroupWork'
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn'
import CodeIcon from '@material-ui/icons/Code'
import LoyaltyIcon from '@material-ui/icons/Loyalty'
import Typography from '@material-ui/core/Typography'

export const MainContainer = styled(Card)({
	marginLeft: '10%',
	width: '100%'
})

export const TextInput = styled(TextField)({width: '100%'})

export const ButtonGroup = styled(Grid)({
	textAlign: 'left',
	marginTop: '1rem'
})

export const ActionButton = styled(Button)({
	backgroundColor: Constant.Color.orange,
	marginRight: '3rem'
})

export const ButtonProgress = styled(CircularProgress)({
	color: green[500],
	top: '0.5rem',
	position: 'relative',
	right: '10rem'
})

export const ArticleContainer = styled('div')({
	marginTop: '2rem',
	width: '100%',
	marginLeft: '10%'
})

export const FootView = styled('div')({
	backgroundColor: Constant.Color.themeColor,
	width: '100%',
	height: '20rem',
	top: '2rem',
	position: 'relative',
	bottom: 0
})

export const FootTextContainer = styled(Grid)({
	color: 'white',
	textAlign: 'center',
	marginTop: '1rem',
})

export const GroupWorkIconView = styled(GroupWorkIcon)({
	position: 'absolute',
	color: '#EF5350',
	margin: '0.2rem auto auto -2rem',
})

export const MonetizationOnIconView = styled(MonetizationOnIcon)({
	position: 'absolute',
	color: '#FFEB3B',
	margin: '0.2rem auto auto -2rem',
})

export const CodeIconView = styled(CodeIcon)({
	position: 'absolute',
	color: '#29B6F6',
	margin: '0.2rem auto auto -2rem',
})

export const LoyaltyIconView = styled(LoyaltyIcon)({
	position: 'absolute',
	color: '#EF5350',
	margin: '0.2rem auto auto -2rem',
})

export const UIDivider = styled('hr')({
	color: 'whitesmoke',
	width: '90%',
})

export const FootTextView = styled(Typography)({
	marginTop: '0.5rem !important',
})

export const FootText = styled('p')({
	color: 'white',
	textAlign: 'center',
	marginTop: '4rem',
})