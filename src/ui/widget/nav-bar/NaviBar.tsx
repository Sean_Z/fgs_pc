import React, {useEffect, useState} from 'react'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import Typography from '@material-ui/core/Typography'
import Drawer from '@material-ui/core/Drawer'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import HomeIcon from '@material-ui/icons/Home'
import HomeWorkIcon from '@material-ui/icons/HomeWork'
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet'
import StoreMallDirectoryIcon from '@material-ui/icons/StoreMallDirectory'
import PeopleAltIcon from '@material-ui/icons/PeopleAlt'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import PopupState, {bindMenu, bindTrigger} from 'material-ui-popup-state'
import {
    DrawerListView,
    DrawerProfileView,
    NaviAvatar,
    NaviDrawerContainer,
    UIAppBar,
    UILogo
} from './NavibarStyledComponent'
import {Links} from '@assets/Constant'
import {useNavigate} from 'react-router'
import {PopupStateStruct} from '../crew/drawer/Drawer'
import {useAppSelector} from '@store/store'

interface DrawerMapIFS {
    component: JSX.Element,
    itemName: string,
    method: VoidFunction
}

export default function NaviBar(): JSX.Element {
    const isShow = useAppSelector((state) => state.userReducer.isShowAvatar)
    const username = useAppSelector((state) => state.userReducer.infiniteFlightUserName)
    const [openDrawer, setOpenDrawer] = useState<boolean>(false)
    const [profileUrl, setProfileUrl] = useState<string>('')
    const navigator = useNavigate()
    const drawerMap: Readonly<DrawerMapIFS[]> = [
        {component: <HomeIcon/>, itemName: 'HomePage', method: () => navigator('/')},
        {component: <PeopleAltIcon/>, itemName: 'AboutView Us', method: () => navigator('/about')},
        {component: <HomeWorkIcon/>, itemName: 'CrewView Center', method: () => navigator('/crew')},
        {component: <StoreMallDirectoryIcon/>, itemName: 'FGS Mall', method: () => setDrawerState()},
        {component: <AccountBalanceWalletIcon/>, itemName: 'Wallet', method: () => setDrawerState()}
    ]

    useEffect(() => {
        const fgsPicUrl = `${Links.fgsStorageBaseUrl}/FGS.png`
        const avatarUrl = `${Links.fgsStorageBaseUrl}/profile/${username}.jpg`
        const profileUrl = username === '' ? fgsPicUrl : avatarUrl
        setProfileUrl(profileUrl)
    })

    const logout = (): void => {
        localStorage.removeItem('nodeToken')
        localStorage.removeItem('refreshToken')
        localStorage.removeItem('accessToken')
        navigator('/login')
    }
    const setDrawerState = () => setOpenDrawer(!openDrawer)
    const NaviAvatarPopup = (props: {popupState: PopupStateStruct}): JSX.Element => {
        const {popupState} = props
        return <>
            <NaviAvatar
                src={profileUrl}
                alt='profile' {...bindTrigger(popupState)}/>
            <Menu {...bindMenu(popupState)}>
                <MenuItem onClick={logout}>Logout</MenuItem>
                <MenuItem onClick={popupState.close}>Profile</MenuItem>
                <MenuItem onClick={() => navigator('/crew')}>Crew Center</MenuItem>
            </Menu>
        </>
    }
    return <>
        <UIAppBar color='primary'>
            <Toolbar>
                <IconButton
                    color='inherit'
                    aria-label='open drawer'
                    edge='start'
                    onClick={setDrawerState}>
                    <MenuIcon/>
                </IconButton>
                <Typography variant='h6' noWrap>
                    Flight Global Studio
                </Typography>
                <UILogo src={Links.fgsLogoHorizontal} alt='logo'/>
                {isShow && <PopupState variant='popover'>
                    {(popupState) => <NaviAvatarPopup popupState={popupState}/>}
                </PopupState>}
            </Toolbar>
        </UIAppBar>
        <Drawer
            variant='temporary'
            anchor='left'
            open={openDrawer}
            onClose={setDrawerState}>
            <NaviDrawerContainer>
                <DrawerProfileView src={profileUrl} alt='profile'/>
                <DrawerListView>
                    {drawerMap.map((item) =>
                        <ListItem key={item.itemName} button onClick={() => item.method()}>
                            <ListItemIcon>{item.component}</ListItemIcon>
                            <ListItemText primary={item.itemName}/>
                        </ListItem>)}
                </DrawerListView>
            </NaviDrawerContainer>
        </Drawer>
    </>
}