import {Constant} from '@assets/Constant'
import drawerImg from '@assets/img/background-img/navbar-side-bg.jpg'
import {styled} from '@material-ui/core'
import AppBar from '@material-ui/core/AppBar'
import Avatar from '@material-ui/core/Avatar'
import List from '@material-ui/core/List'
import {theme} from '../../theme/Theme'

export const UIAppBar = styled(AppBar)({
	backgroundColor: Constant.Color.themeColor,
	zIndex: theme.zIndex.drawer + 1
})

export const NaviDrawerContainer = styled('div')({
	backgroundImage: `url(${drawerImg})`,
	height: '100%',
	color: 'white'
})

export const UILogo = styled('img')({
	position: 'fixed',
	zIndex: 5,
	width: 240,
	marginLeft: '15rem',
	paddingTop: '1rem'
})

export const NaviAvatar = styled(Avatar)({
	margin: '0 0 0 auto'
})

export const DrawerProfileView = styled(Avatar)({
	margin:' 0.5rem auto 0.5rem auto',
	zoom: '190%'
})

export const DrawerListView = styled(List)({
	width: '15rem',
	height: '100%'
})