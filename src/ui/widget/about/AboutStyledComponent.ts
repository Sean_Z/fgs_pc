import {styled} from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import Card from '@material-ui/core/Card'

export const AboutTitleTextView = styled(Typography)({
	fontSize: '2rem',
	fontFamily: '-apple-system, BlinkMacSystemFont, \'Segoe UI\', \'Roboto\', \'Oxygen\', \'Ubuntu\', \'Cantarell\', \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif',
	color: 'antiquewhite',
	marginTop: '3rem'
})

export const JoinButton = styled(Button)({
	background: '#ff851b ',
	color: 'white',
	marginTop: '1rem',
	width: '10rem'
})

export const UserCardContainer = styled(Card)({
	height: 'auto',
	width: '400px',
	display: 'inline-block',
	margin:' 1rem 4rem 2rem 4rem'
})

export const UserMapView = styled('div')({
	width: '400px',
	height: '280px',
	padding: 0,
	margin: 0,
	position: 'absolute',
	zIndex: 4
})

export const SwiperContainer = styled('div')({
	marginTop: '-1rem'
})

export const MainTextView = styled(Typography)({
	margin: '5rem 1rem 2rem 0',
	color: 'antiquewhite'
})

export const ContentTextView = styled(Typography)({
	margin: '2rem 1rem 2rem 0',
	fontSize: '1.4rem',
	color: 'antiquewhite'
})