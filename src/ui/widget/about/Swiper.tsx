import {ContentTextView, MainTextView, SwiperContainer} from './AboutStyledComponent'

/**
 * @method 此组件为about页面的跑马灯模块，主要自动滑动播放成员生活中的照片（照片为成员所上传）
 * @Author Sean_Zhang
 */

export default function Swiper(): JSX.Element {

	return (
		<SwiperContainer>
			<MainTextView variant='h3' gutterBottom>As our president said</MainTextView>
			<ContentTextView variant='body1' gutterBottom>
				The top goal of FGS is changing
				our property from game to real life
			</ContentTextView>
		</SwiperContainer>
	)
}