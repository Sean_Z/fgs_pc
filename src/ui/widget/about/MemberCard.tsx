import React, {useEffect, useState} from 'react'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardHeader from '@material-ui/core/CardHeader'
import CardMedia from '@material-ui/core/CardMedia'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import CardActions from '@material-ui/core/CardActions'
import Button from '@material-ui/core/Button'
import {UserCardContainer, UserMapView} from './AboutStyledComponent'
import {Links} from '@assets/Constant'
import {FgsService} from '@http/FgsService'
import {useDispatch} from 'react-redux'
import {uiActions} from '@store/reducers/UIReducer'
import {MemberListIFSList, MemberListRes} from '@http/entity/response/MemberListRes'

export default function MemberCard(): JSX.Element {
    const [memberList, setMemberList] = useState<MemberListRes>()
    const [mapId, setMapId] = useState<string>('')
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(uiActions.setLoading(true))
        getUserList()
            .then(res => setMemberList(res))
            .finally(() => dispatch(uiActions.setLoading(false)))
    }, [])

    const mapInfo = (data: MemberListIFSList): void => {
        mapId !== '' && mapId === data.username ? setMapId('') : setMapId(data.username)
    }

    const getUserList = async (): Promise<MemberListRes> => {
        return await FgsService.getFgsUserList()
    }

    return <>{
        memberList?.list.map((data: MemberListIFSList) => {
            return <UserCardContainer key={data.userId}>
                <CardActionArea onClick={() => mapInfo(data)}>
                    <CardHeader
                        title={data.username}
                        subheader={new Date(data.createTime).toString()}/>
                    {mapId === data.username ?
                        <CardMedia style={{height: '280px'}}>
                            <UserMapView>
                                <div id={`OMSMAP${data.username}`}/>
                            </UserMapView>
                        </CardMedia> : <CardMedia
                            style={{height: '280px'}}
                            image={`${Links.fgsStorageBaseUrl}profile/${data.username}.jpg`}/>
                    }
                    <CardContent>
                        <Typography gutterBottom variant={'h5'} component={'h2'}>
                            {data.position}
                        </Typography>
                        <Typography variant={'body2'} color={'textSecondary'} component={'p'}>
                            {data.position}
                        </Typography>
                    </CardContent>
                </CardActionArea>
                <CardActions>
                    <Button size={'small'} color={'primary'}>
                        Learn More
                    </Button>
                </CardActions>
            </UserCardContainer>
        })
    }</>
}