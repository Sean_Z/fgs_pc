import React from 'react'
import PersonAddIcon from '@material-ui/icons/PersonAdd'
import {AboutTitleTextView, JoinButton} from './AboutStyledComponent'
import {useNavigate} from 'react-router'
import {useDispatch} from 'react-redux'
import {userAction} from '@store/reducers/UserReducer'

/**
 * @method 此组件为about页面的上层主干信息
 * @Author Sean_Zhang
 */

export default function Title (): JSX.Element {

	const history = useNavigate()
	const dispatcher = useDispatch()

	/**
	 * @method toApplication
	 * @description 跳转到申请页
	 */
	const toApplication = (): void => {
		dispatcher(userAction.setIsShowAvatar(false))
		history('/application')
	}

	return <AboutTitleTextView variant='h5' gutterBottom>
		Since 2018 until now, 20 members have joined FGS<br/>
		Will you be the next one ?<br/>
		<JoinButton
			variant='contained'
			onClick={toApplication}
			endIcon={<PersonAddIcon/>}>
			Join Us!
		</JoinButton>
	</AboutTitleTextView>
}
