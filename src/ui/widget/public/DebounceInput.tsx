import {TextField} from '@material-ui/core'
import React from 'react'
import _ from 'lodash'

interface Props {}
interface InnerState {
    val: string
}
export default class DebounceInput extends React.Component<Props, InnerState> {
    constructor(props: Props) {
        super(props)
        this.state = {
            val: ''
        }
    }

    protected doSomething(): void {
        console.log(this.state.val)
    }

    private startInput (e: React.ChangeEvent<HTMLInputElement>): void {
        const value = e.target.value
        this.setState({val: value})
        if (this.doSomething) {
            this.doSomething = _.debounce(this.doSomething, 1000)
        }
        this.doSomething()
    }

    public render(): JSX.Element {
        return <TextField
            fullWidth={true}
            value={this.state.val}
            style={{background: 'wheat'}}
            label={'test'}
            onInput={(e: React.ChangeEvent<HTMLInputElement>) => this.startInput(e)}
        />
    }
}