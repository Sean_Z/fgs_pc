import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardContent from '@material-ui/core/CardContent'
import React from 'react'

export default function CardContainer(props: {children: React.ReactNode}): JSX.Element {
    return <Card>
        <CardActionArea>
            <CardContent>
                {props.children}
            </CardContent>
        </CardActionArea>
    </Card>
}