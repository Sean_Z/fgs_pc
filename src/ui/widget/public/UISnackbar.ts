import {useDispatch} from 'react-redux'
import {uiActions} from '@store/reducers/UIReducer'

export function useErrorSnackBar(): (msg: string) => void {
    const dispatch = useDispatch()
    return (msg: string): void => {
        dispatch(uiActions.setSnackBarMessage({open: true, messageType: 'error', content: msg}))
        setTimeout(() => {
            dispatch(uiActions.setSnackBarMessage({open: false, messageType: 'success', content: msg}))
        }, 2000)
    }
}

export function useSuccessSnackBar(): (msg: string) => void {
    const dispatch = useDispatch()
    return (msg: string): void => {
        dispatch(uiActions.setSnackBarMessage({open: true, messageType: 'success', content: msg}))
        setTimeout(() => {
            dispatch(uiActions.setSnackBarMessage({open: false, messageType: 'success', content: msg}))
        }, 2000)
    }
}