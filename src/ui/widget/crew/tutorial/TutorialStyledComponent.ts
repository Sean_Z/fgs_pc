import backgroundImg from '@assets/img/background-img/radar.jpg'
import {styled} from '@material-ui/core'
import {theme} from '../../../theme/Theme'

export const BlogImageView = styled('div')({
	width: '98%',
	padding: '1rem',
	height: '20rem',
	borderRadius: '0.5rem',
	borderBottomRightRadius: '0',
	borderBottomLeftRadius: '0',
	display: 'block',
	margin: '1rem auto auto auto',
	backgroundImage: `url(${backgroundImg})`
})

export const ImageTextView = styled('p')({
	display: 'inline',
	position: 'absolute',
	zIndex: 2,
	color: 'white',
	fontSize: '2rem',
	margin: '1.5rem'
})

export const TabView = styled('div')({
	backgroundColor: theme.palette.background.paper,
	width: '98%',
	margin: '0 auto'
})

export const IframeView = styled('iframe')({
	border: 'none'
})