import {useState} from 'react'
import {BlogImageView, IframeView, ImageTextView, TabView} from './TutorialStyledComponent'
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography'
import PropTypes from 'prop-types'
import {useTheme} from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'
import {TabContext} from '@material-ui/lab'
import SwipeViews from 'react-swipeable-views'
import {Constant} from '@assets/Constant'

function TabPanel(props) {

	const {children, value, index, ...other} = props

	return (
		<div
			role="tabpanel"
			hidden={value !== index}
			id={`full-width-tabpanel-${index}`}
			aria-labelledby={`full-width-tab-${index}`}
			{...other}
		>
			{value === index &&
			<Box p={3}>
				<Typography>{children}</Typography>
			</Box>}
		</div>
	)
}

TabPanel.propTypes = {
	children: PropTypes.node,
	index: PropTypes.any.isRequired,
	value: PropTypes.any.isRequired,
}

export default function Tutorial (){
	const [value, setValue] = useState(0)
	const theme = useTheme()

	/**
	 * @method handleChange
	 * @description 切换页码
	 * @param event
	 * @param newValue
	 */
	const handleChange = (event, newValue) => setValue (newValue)

	/**
	 * @method handleChangeIndex
	 * @description 设置页码
	 * @param index
	 */
	const handleChangeIndex = (index) => setValue (index)

	return (
		<>
			<BlogImageView>
				<ImageTextView>Hello here is FGS Pilot Tutorial</ImageTextView>
			</BlogImageView>
			<TabView>
				<AppBar position="static" color="default">
					<Tabs
						value={value}
						onChange={handleChange}
						indicatorColor='primary'
						textColor='primary'
						variant='fullWidth'
						aria-label='full width tabs example'>
						<Tab label="News" id={'0'} aria-controls={'full-width-tab panel-0'} />
						<Tab label="ATC" id={'1'} aria-controls={'full-width-tab panel-1'} />
						<Tab label="Pilot" id={'2'} aria-controls={'full-width-tab panel-2'} />
					</Tabs>
				</AppBar>
				<SwipeViews
					axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
					index={value}
					onChangeIndex={handleChangeIndex}>
					<TabContext index={0} dir={theme.direction} value={''}>
						<IframeView src={Constant.ifBlog} width={'100%'} height={1000} title={'blog'} />
					</TabContext>
					<TabContext index={1} dir={theme.direction} value={''}>
						<IframeView src={Constant.ifAtc} width={'100%'} height={'100%'} title={'atc'} />
					</TabContext>
					<TabContext index={2} dir={theme.direction} value={''}>
						<IframeView src={Constant.ifPilot} width={'100%'} height={'100%'} title={'pilot'} />
					</TabContext>
				</SwipeViews>
			</TabView>
		</>
	)
}
