import CardActionArea from '@material-ui/core/CardActionArea'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import CardActions from '@material-ui/core/CardActions'
import Button from '@material-ui/core/Button'
import Card from '@material-ui/core/Card'
import React, {useEffect, useState} from 'react'
import {CardInfo, ProfileImgView, RouteInfoTextView} from './DashboardStyledComponent'
import {Links} from '@assets/Constant'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import TextField from '@material-ui/core/TextField'
import DialogActions from '@material-ui/core/DialogActions'
import Dialog from '@material-ui/core/Dialog'
import copy from 'copy-to-clipboard'
import {FgsService} from '@http/FgsService'
import {useDispatch} from 'react-redux'
import {userAction} from '@store/reducers/UserReducer'
import {useAppSelector} from '@store/store'
import {InfiniteFlightUser} from '@http/entity/response/InfiniteFlightUserRes'
import {useErrorSnackBar, useSuccessSnackBar} from '../../public/UISnackbar'

export default function ProfileCard(): JSX.Element {
    // 请求航路对话框
    const [isOpenDialog, setIsOpenDialog] = useState<boolean>(false)
    // 航路数据
    const [navData, setNavData] = useState<string>('')
    // 航线距离
    const [distance, setDistance] = useState<string>('')
    // 按钮状态
    const [btnStatus, setBtnStatus] = useState({state: false, name: 'FETCH'})
    const [infiniteFlightUser, setInfiniteFlightUser] = useState<InfiniteFlightUser>()
    // 从store中取出用户名渲染main info部分中的用户名
    const username = useAppSelector((state) => state.userReducer.infiniteFlightUserName)
    const ownRoute = useAppSelector((state) => state.fmsReducer.currentFlightRoute)
    const profilePic: string = username === undefined ? `${Links.fgsStorageBaseUrl}/FGS.png` : `https://fgs-1300812707.cos.ap-shanghai.myqcloud.com/profile/${username}.jpg`
    const dispatch = useDispatch()
    const successSnackBar = useSuccessSnackBar()
    const errorSnackBar = useErrorSnackBar()

    useEffect(() => {
        ProfileCardController.getUserInfo().catch()
    }, [])
    // 请求航路数据
    const reqNavData: { fromICAO: string; toICAO: string } = {
        fromICAO: '',
        toICAO: ''
    }

    class ProfileCardController {
        /**
         * @method fetchData
         * @description 根据传入的起降地ICAO代码获取在线航路，获取成功之后转换按钮为复制按钮
         * @return void
         */
        public static async fetchFplData(): Promise<void> {
            setBtnStatus({state: true, name: 'FETCHING'})
            if (reqNavData.fromICAO === '' || reqNavData.toICAO === '' || !/^[A-Za-z0-9]+$/.test(reqNavData.toICAO) || !/^[A-Za-z0-9]+$/.test(reqNavData.fromICAO)) {
                errorSnackBar('check input data')
                setBtnStatus({state: false, name: 'FETCHING'})
            } else {
                const res = await FgsService.fetchFlightPlan(reqNavData)
                setBtnStatus({state: false, name: 'COPY'})
                setNavData(res.routeInfo)
                setDistance(`${res.distance.toFixed(2)}nms`)
            }
        }

        /**
         * 获取用户信息
         */
        public static async getUserInfo(): Promise<void> {
            const user = await FgsService.getUserInfo(username)
            dispatch(userAction.setUser(user))
            setInfiniteFlightUser(user)
        }

        /**
         * @method closeFrom
         * @description 关闭并清空表单
         * @return void
         */
        public static closeDialog(): void {
            setBtnStatus({state: false, name: 'FETCH'})
            setNavData('')
            setDistance('')
            setIsOpenDialog(false)
        }

        /**
         * @method copyData
         * @description 赋值航路,复制完成关闭对话框
         * @return void
         */
        public static copyData(): void {
            copy(navData)
            setIsOpenDialog(false)
            ProfileCardController.closeDialog()
        }
    }

    const rawFlightTime = infiniteFlightUser?.flightTime ?? 0
    const flightTimeHr = (rawFlightTime / 60).toFixed(0)
    const flightTimeMin = rawFlightTime % 60
    const flightTime = `${flightTimeHr}Hr ${flightTimeMin}Min`
    return (
        <>
            <Card>
                <CardActionArea>
                    <ProfileImgView image={profilePic}/>
                    <CardContent>
                        <Typography gutterBottom variant='h5' component='h2'>
                            {username}
                        </Typography>
                        <CardInfo>
                            <Grid container spacing={1}>
                                <Grid item xs={6}>
                                    Flight Time: {flightTime}
                                </Grid>
                                <Grid item xs={6}>
                                    XP: {infiniteFlightUser?.xp ?? 0}
                                </Grid>
                                <Grid item xs={6}>
                                    Flights: {infiniteFlightUser?.onlineFlights}
                                </Grid>
                                <Grid item xs={6}>
                                    Grade: {infiniteFlightUser?.grade ?? 1}
                                </Grid>
                            </Grid>
                        </CardInfo>
                    </CardContent>
                </CardActionArea>
                <CardActions>
                    <Button size='small' color='primary' onClick={() => setIsOpenDialog(true)}>
                        general fpl
                    </Button>
                    <Button size='small' color='primary' onClick={() => {
                        copy(ownRoute?.routeInfo ?? '')
                        successSnackBar('copy own route successfully!')}}>
                        copy own route
                    </Button>
                </CardActions>
            </Card>
            <Dialog open={isOpenDialog} onClose={() => setIsOpenDialog(false)} aria-labelledby='form-dialog-title'>
                <DialogTitle>Fetch Flight Plan</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        To fetch flight plan in this section, please enter your departure ICAO code and arrival ICAO
                        code here. We will send Navigation
                        data.
                    </DialogContentText>
                    <Grid container spacing={1}>
                        <Grid item xs={6}>
                            <TextField
                                autoFocus
                                margin='dense'
                                label='Departure'
                                type='text'
                                onChange={(event) => reqNavData.fromICAO = event.target.value.toUpperCase()}/>
                            <br/>
                            <TextField
                                margin='dense'
                                label='Arrival'
                                type='text'
                                onChange={(event) => reqNavData.toICAO = event.target.value.toUpperCase()}/>
                        </Grid>
                        <Grid item xs={6} style={{marginTop: '1rem'}}>
                            <Typography variant='subtitle1'>Route: {
                                <RouteInfoTextView>{navData}</RouteInfoTextView>}</Typography><br/>
                            <Typography variant='subtitle1'>Distance: {distance}</Typography>
                        </Grid>
                    </Grid>
                </DialogContent>
                <DialogActions>
                    <Button
                        onClick={btnStatus.name === 'FETCH' ? ProfileCardController.fetchFplData : ProfileCardController.copyData}
                        disabled={btnStatus.state}
                        color='primary'>
                        {btnStatus.name}
                    </Button>
                    <Button onClick={ProfileCardController.closeDialog} color='primary'>
                        CLOSE
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    )
}