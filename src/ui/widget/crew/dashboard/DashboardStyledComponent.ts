import {Constant} from '@assets/Constant'
import {styled} from '@material-ui/core'
import Paper from '@material-ui/core/Paper'
import CardMedia from '@material-ui/core/CardMedia'
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography'

export const RCFMainViewContainer = styled(Paper)({
    width: '100%'
})

export const ProfileImgView = styled(CardMedia)({
    objectFit: 'cover',
    height: 300
})

export const CardInfo = styled('div')({
    color: Constant.Color.themeColor,
    fontSize: '1rem',
})

export const RouteInfoTextView = styled('div')({
    maxWidth: '30rem',
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis'
})

export const TableLoadingContainer = styled(Box)({
    display: 'flex',
    justifyContent: 'center',
    width: '100%',
    position: 'absolute',
    height: '100%',
    alignItems: 'center',
    marginTop: '1rem',
    backgroundColor: 'white',
    opacity: 0.6
})

export const TimeTextView = styled(Typography)({
    color: '#00a152'
})

export const DeptTimeTextView = styled(Typography)({})

export const FlightDetailContainer = styled('div')({
    marginTop: 16
})

export const FlightDetailTextView = styled('div')({
    display: 'flex',
    color: 'white',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    margin: '1rem auto'
})

export const FlightDetailItems = styled('div')({
    display: 'flex',
    justifyContent: 'space-around'
})