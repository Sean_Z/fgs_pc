import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardContent from '@material-ui/core/CardContent'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import {DeptTimeTextView, TimeTextView} from './DashboardStyledComponent'
import Avatar from '@material-ui/core/Avatar'
import {CalculateMethod, Tools} from '@core/Tools'
import moment from 'moment/moment'
import {useEffect, useMemo, useState} from 'react'
import {LinearProgress} from '@material-ui/core'
import CardMedia from '@material-ui/core/CardMedia'
import {Links} from '@assets/Constant'
import {useAppSelector} from '@store/store'
import {AirwayLineRes} from '@http/entity/response/AirwayLineRes'

export default function CurrentFlightMainInfo(): JSX.Element {
    const currentFlightDetail = useAppSelector((state) => state.fmsReducer.currentFlightDetail)
    const lastFlightLog = useAppSelector((state) => state.fmsReducer.lastFlightLog)
    const currentFlightPlan = useAppSelector((state) => state.fmsReducer.currentFlightRoute)
    const currentAirwayPoints = useAppSelector((state) => state.fmsReducer.currentAirway)
    const getAirlineIcon = Tools.getAirlineIcon(currentFlightDetail?.livery[0].liveryName ?? 'unknown')
    const airlineIconAvatarStyle = {maxWidth: '4rem', minWidth: '4rem', height: 'auto', margin: '0 auto'}
    const [deptTime, setDeptTime] = useState<string>('')
    const [destTime, setDestTime] = useState<string>('')
    const setScheduleTime = async (): Promise<void> => {
        setDeptTime(calculateDeptTime(currentAirwayPoints))
        setDestTime(calculateDestTime())
    }
    useEffect(() => {
        setScheduleTime().catch()
    }, [currentFlightDetail, lastFlightLog, currentFlightPlan?.wholeRangePosition, currentAirwayPoints])
    const wholeRange = CalculateMethod.entireDistance(currentFlightPlan?.wholeRangePosition ?? [])
    const remainRange = CalculateMethod.calculateRemainRange(currentFlightPlan?.wholeRangePosition ?? [], currentAirwayPoints ?? [])
    const calculateDestTime = (): string => {
        const _remainRange = CalculateMethod.calculateRemainRange(currentFlightPlan?.wholeRangePosition ?? [], currentAirwayPoints ?? [])
        const remainMinutes = CalculateMethod.calculateRemainMinutes(_remainRange, currentFlightDetail?.speed ?? 0).toFixed(0)
        return moment(new Date()).add(remainMinutes, 'minutes').format('MM/DD HH:MM')
    }
    const calculateDeptTime = (response: AirwayLineRes[]): string => {
        const departureTime = response.filter(element => element.altitude > response[0].altitude)[0]?.time ?? ''
        return moment(departureTime).format('MM/DD HH:MM')
    }
    const distanceRemainMemo = useMemo(() => (wholeRange - remainRange) / wholeRange * 100, [remainRange, wholeRange])
    const dest = lastFlightLog?.destinationAirport ? lastFlightLog.destinationAirport : currentFlightPlan?.arrival
    const callSignText = `Callsign: ${currentFlightDetail?.callSign}`
    return currentFlightDetail && lastFlightLog ? <Card>
        <CardActionArea>
            <CardContent>
                <Grid container spacing={0}>
                    <Grid item xs={12}>
                        <Avatar
                            variant={'square'}
                            style={airlineIconAvatarStyle}
                            src={getAirlineIcon}/>
                    </Grid>
                </Grid>
                <Grid container>
                    <Grid item xs={4}>
                        <Typography variant={'h5'}>Departure: {lastFlightLog.originAirport}</Typography>
                        <TimeTextView variant={'subtitle1'}>EN ROUTE AND ON TIME</TimeTextView>
                        <DeptTimeTextView variant={'body1'}>Departure at {deptTime}</DeptTimeTextView>
                    </Grid>
                    <Grid item xs={4} style={{margin: 'auto 0'}}>
                        <LinearProgress variant='determinate' value={distanceRemainMemo}/>
                        <Typography style={{textAlign: 'center'}} variant={'subtitle2'}>Distance
                            remain {remainRange.toFixed(2)} nms</Typography>
                    </Grid>
                    <Grid item xs={4} style={{display: 'flex', alignItems: 'end', flexDirection: 'column'}}>
                        <Typography variant={'h5'}>Destination: {dest}</Typography>
                        <Typography variant={'body1'}>{callSignText}</Typography>
                        <DeptTimeTextView variant={'body1'}>Arrival at {destTime}</DeptTimeTextView>
                    </Grid>
                </Grid>
            </CardContent>
            <CardMedia style={{height: 400}}
                       image={Links.liveryPic(currentFlightDetail.aircraftId, currentFlightDetail.liveryId)}/>
        </CardActionArea>
    </Card> : <></>
}