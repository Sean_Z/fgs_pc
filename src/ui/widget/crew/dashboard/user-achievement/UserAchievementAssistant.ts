import {UserGradeRes} from '@http/entity/response/UserGradeRes'

export const buildGradeItemRequirement = (userGrade: UserGradeRes): GradeRequirement => {
    const totalFlightTimeIndex = 0
    const totalLandingIndex = 1
    const totalXPIndex = 2
    const totalFlightTimeIn90DaysIndex = 9
    const totalLandingIn90DaysIndex = 10
    const {grades} = userGrade!.gradeDetails
    const totalFlightTimeStepList: string[] = []
    const totalXPStepList: string[] = []
    const totalLandingStepList: string[] = []
    const totalLandingIn90DaysStepList: string[] = []
    const totalFlightTimeIn90DaysStepList: string[] = []
    let result: GradeRequirement

    for (let index = 0; index < grades.length; index++) {
        const grade = grades[index]
        const totalFlightRequirements = grade.rules.filter(element => element.ruleIndex === totalFlightTimeIndex)
        const totalXPRequirements = grade.rules.filter(element => element.ruleIndex === totalXPIndex)
        const totalLandingRequirements = grade.rules.filter(element => element.ruleIndex === totalLandingIndex)
        const totalLandingIn90DaysRequirements = grade.rules.filter(element => element.ruleIndex === totalLandingIn90DaysIndex)
        const totalFlightTimeIn90DaysRequirements = grade.rules.filter(element => element.ruleIndex === totalFlightTimeIn90DaysIndex)

        totalFlightRequirements.forEach((element) => totalFlightTimeStepList.push(`G${index+1} ${element.referenceValue} hrs`))
        totalXPRequirements.forEach((element) => totalXPStepList.push(`G${index+1} ${element.referenceValue} XP`))
        totalLandingRequirements.forEach((element) => totalLandingStepList.push(`G${index+1} ${element.referenceValue}`))
        totalLandingIn90DaysRequirements.forEach((element) => totalLandingIn90DaysStepList.push(`G${index+1} ${element.referenceValue}`))
        totalFlightTimeIn90DaysRequirements.forEach((element) => totalFlightTimeIn90DaysStepList.push(`G${index+1} ${element.referenceValue}`))
        if (index === grades.length - 1) {
            result = {
                totalFlightTimeRequirement: {
                    stepList: totalFlightTimeStepList,
                    userValue: Math.round(totalFlightRequirements[0].userValue),
                    nextIndex: totalFlightRequirements.findIndex(element => element.referenceValue > totalFlightRequirements[0].userValue)
                },
                totalLandingRequirement: {
                    stepList: totalXPStepList,
                    userValue: totalXPRequirements[0].userValue,
                    nextIndex: totalXPRequirements.findIndex(element => element.referenceValue > totalXPRequirements[0].userValue)
                },
                totalXPRequirement: {
                    stepList: totalLandingStepList,
                    userValue: totalLandingRequirements[0].userValue,
                    nextIndex: totalLandingRequirements.findIndex(element => element.referenceValue > totalLandingRequirements[0].userValue)
                },
                totalLandingIn90DaysRequirement: {
                    stepList: totalLandingIn90DaysStepList,
                    userValue: totalLandingIn90DaysRequirements[0].userValue,
                    nextIndex: totalLandingIn90DaysRequirements.findIndex(element => element.referenceValue > totalLandingIn90DaysRequirements[0].userValue)
                },
                totalFlightTimeIn90DaysRequirement: {
                    stepList: totalFlightTimeIn90DaysStepList,
                    userValue: Math.round(totalFlightTimeIn90DaysRequirements[0].userValue),
                    nextIndex: totalFlightTimeIn90DaysRequirements.findIndex(element => element.referenceValue > totalFlightTimeIn90DaysRequirements[0].userValue)
                },
            }
        }
    }
    return result!
}

export interface GradeRequirement {
    readonly totalFlightTimeRequirement: ListValue
    readonly totalLandingRequirement: ListValue
    readonly totalXPRequirement: ListValue
    readonly totalLandingIn90DaysRequirement: ListValue
    readonly totalFlightTimeIn90DaysRequirement: ListValue
}

export interface ListValue {
    stepList: string[]
    userValue: number | string
    nextIndex: number
}