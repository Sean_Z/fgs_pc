import Card from '@material-ui/core/Card'
import {useEffect, useState} from 'react'
import {FgsService} from '@http/FgsService'
import CardHeader from '@material-ui/core/CardHeader'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import {Accordion, AccordionDetails, AccordionSummary} from '@mui/material'
import {makeStyles} from '@mui/styles'
import {ExpandMore} from '@material-ui/icons'
import Stepper from '@material-ui/core/Stepper'
import Step from '@material-ui/core/Step'
import StepLabel from '@material-ui/core/StepLabel'
import {Constant} from '@assets/Constant'
import {useAppSelector} from '@store/store'
import {UserGradeRes} from '@http/entity/response/UserGradeRes'
import {buildGradeItemRequirement} from './UserAchievementAssistant'

const useStyles = makeStyles(() => ({
    root: {
        '& .MuiStepIcon-active': {color: Constant.Color.orange},
        '& .MuiStepIcon-completed': {color: Constant.Color.green},
    }
}))
export default function UserAchievement(): JSX.Element {
    const style = useStyles()
    const userName = useAppSelector<string>((state) => state.userReducer.infiniteFlightUserName)
    const infiniteFlightUserId = useAppSelector<string>((state) => state.userReducer.infiniteFlightUserId)
    const [userGrade, setUserGrade] = useState<UserGradeRes>()

    useEffect(() => {
        getUserGrade().catch()
    }, [])

    const getUserGrade = async (): Promise<void> => {
        const userGrade = await FgsService.getUserGrade(infiniteFlightUserId)
        setUserGrade(userGrade)
    }

    const AccordionItem = (props: AccordingItemProps): JSX.Element => {
        const {title, stepList, userValue, nextIndex} = props
        return <Accordion expanded={true}>
            <AccordionSummary expandIcon={<ExpandMore/>}>
                <Typography variant={'h6'}>
                    {title}
                </Typography>
                <Typography
                    variant={'h6'}
                    style={{textAlign: 'center', left: '19rem', position: 'absolute'}}>
                    <>{userValue}</>
                </Typography>
            </AccordionSummary>
            <AccordionDetails>
                <Stepper className={style.root} activeStep={nextIndex} alternativeLabel>
                    {stepList.map((label) => (
                        <Step key={label}>
                            <StepLabel>{label}</StepLabel>
                        </Step>))}
                </Stepper>
            </AccordionDetails>
        </Accordion>
    }

    return userGrade ?
        <Card>
            <CardHeader title={userName}/>
            <CardActionArea>
                <CardContent>
                    <AccordionItem
                        title={'Landing in 90 Days'}
                        userValue={buildGradeItemRequirement(userGrade).totalLandingIn90DaysRequirement.userValue}
                        stepList={buildGradeItemRequirement(userGrade).totalLandingIn90DaysRequirement.stepList}
                        nextIndex={buildGradeItemRequirement(userGrade).totalLandingIn90DaysRequirement.nextIndex}/>
                    <AccordionItem
                        title={'Flight Time in 90 Days'}
                        userValue={buildGradeItemRequirement(userGrade).totalFlightTimeIn90DaysRequirement.userValue}
                        stepList={buildGradeItemRequirement(userGrade).totalFlightTimeIn90DaysRequirement.stepList}
                        nextIndex={buildGradeItemRequirement(userGrade).totalFlightTimeIn90DaysRequirement.nextIndex}/>
                    <AccordionItem
                        title={'Flight time'}
                        userValue={buildGradeItemRequirement(userGrade).totalFlightTimeRequirement.userValue}
                        stepList={buildGradeItemRequirement(userGrade).totalFlightTimeRequirement.stepList}
                        nextIndex={buildGradeItemRequirement(userGrade).totalFlightTimeRequirement.nextIndex}/>
                    <AccordionItem
                        title={'Total Landing'}
                        userValue={buildGradeItemRequirement(userGrade).totalXPRequirement.userValue}
                        stepList={buildGradeItemRequirement(userGrade).totalXPRequirement.stepList}
                        nextIndex={buildGradeItemRequirement(userGrade).totalXPRequirement.nextIndex}/>
                    <AccordionItem
                        title={'Total XP'}
                        userValue={buildGradeItemRequirement(userGrade).totalLandingRequirement.userValue}
                        stepList={buildGradeItemRequirement(userGrade).totalLandingRequirement.stepList}
                        nextIndex={buildGradeItemRequirement(userGrade).totalLandingRequirement.nextIndex}/>
                </CardContent>
            </CardActionArea>
        </Card> : <></>
}

interface AccordingItemProps {
    readonly title: string
    readonly stepList: string[]
    readonly userValue: string | number
    readonly nextIndex: number
}