import {EChartsOption} from 'echarts'

interface FlightFrequencyConfigParam {
    timeList: string[]
    flightList: number[]
    landingList: number[]
}
export const flightFrequencyConfig = (param: FlightFrequencyConfigParam): EChartsOption => {
    const {timeList, landingList, flightList} = param
    const maxFlightYAxisValue = JSON.parse(JSON.stringify(flightList)).sort().at(-1) + 10
    const maxLandingYAxisValue = JSON.parse(JSON.stringify(landingList)).sort().at(-1) + 10
    return {
        title: {
            padding: [5, 10],
            text: 'Flight Frequency',
        },
        legend: {
            padding: [5, 10],
            data: ['Flight', 'Landing'],
            left: 'center',
        },
        tooltip: {
            trigger: 'axis'
        },
        xAxis: [{
            type: 'category',
            boundaryGap: true,
            data: timeList
        }],
        yAxis: [
            {
                type: 'value',
                axisLabel: {formatter: '{value}'},
                maxInterval: maxFlightYAxisValue
            },
            {
                type: 'value',
                axisLabel: {formatter: '{value} landing'},
                maxInterval: maxLandingYAxisValue
            }],
        series: [
            {
                name: 'Flight',
                type: 'bar',
                stack: 'Flight',
                data: flightList,
                yAxisIndex: 0,
                itemStyle: {color: '#FFF9C4'}
            },
            {
                name: 'Landing',
                type: 'bar',
                stack: 'Landing',
                data: landingList,
                yAxisIndex: 0,
                itemStyle: {color: '#90CAF9'}
            }
        ]
    }
}