import React from 'react'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardContent from '@material-ui/core/CardContent'
import {FlightDetailContainer, FlightDetailItems, FlightDetailTextView} from './DashboardStyledComponent'
import Typography from '@material-ui/core/Typography'
import copy from 'copy-to-clipboard'
import StatusCharts from '../onlinemap/StatusCharts'
import {useAppSelector} from '@store/store'
import {useSuccessSnackBar} from '../../public/UISnackbar'

export default function CurrentFlightDetail(): JSX.Element {
    const currentFlightDetail = useAppSelector((state) => state.fmsReducer.currentFlightDetail)
    const lastFlightLog = useAppSelector((state) => state.fmsReducer.lastFlightLog)
    const onlinePlan = useAppSelector(state => state.fmsReducer.currentFlightRoute)
    const showSuccessSnackBar = useSuccessSnackBar()
    const FlightDataItem = (props: {content: string, title?: string}): JSX.Element => {
        return <FlightDetailTextView>
            <Typography variant={'h6'}>
                {props.content}
            </Typography>
            <Typography variant={'subtitle1'}>
                {props?.title ?? ''}
            </Typography>
        </FlightDetailTextView>
    }
    const copyRoute = (): void => {
        showSuccessSnackBar('copied')
        copy(onlinePlan?.routeInfo ?? '')
    }
    return currentFlightDetail && lastFlightLog ?
        <FlightDetailContainer>
            <Card style={{backgroundColor: '#2c343c'}}>
                <CardActionArea>
                    <CardContent>
                        <FlightDataItem content={`Executing Flight ${currentFlightDetail.callSign}`} />
                        <FlightDetailItems>
                            <FlightDataItem title={'Speed'} content={`${currentFlightDetail.speed.toFixed(0)} kts`} />
                            <FlightDataItem title={'ALT'} content={`${currentFlightDetail.altitude.toFixed(0)} kts`} />
                            <FlightDataItem title={'VS'} content={`${currentFlightDetail.verticalSpeed.toFixed(0)} kts`} />
                        </FlightDetailItems>
                    </CardContent>
                    <div style={{display: 'flex', justifyContent: 'center'}}>
                        <StatusCharts
                            livery={currentFlightDetail.livery[0].liveryName}
                            callSign={currentFlightDetail.callSign}/>
                    </div>
                    <Typography onClick={copyRoute} style={{color: 'white', padding: 16}}>
                        {onlinePlan?.routeInfo}
                    </Typography>
                </CardActionArea>
            </Card>
        </FlightDetailContainer> : <></>
}