import React, {lazy} from 'react'
import Grid from '@material-ui/core/Grid'
import CurrentFlightMap from '../onlinemap/CurrentFlightMap'
import CurrentFlightMainInfo from './CurrentFlightMainInfo'
import CurrentFlightDetail from './CurrentFlightDetail'
import UserAchievement from './user-achievement/UserAchievement'
import {useAppSelector} from '@store/store'

const MainInfo = lazy(() => import('./MainInfo'))
const ProfileCard = lazy(() => import('./ProfileCard'))
const RecentFlightsTable = lazy(() => import('./RecentFlightsTable'))

export default function DashBoard (): JSX.Element {
	const currentFlightDetail = useAppSelector((state) => state.fmsReducer.currentFlightDetail)
	const lastFlightLog = useAppSelector((state) => state.fmsReducer.lastFlightLog)

	return (
		<Grid style={{padding: 16}} container spacing={2}>
			<Grid item xs={12}>
				<MainInfo />
			</Grid>
			<Grid item xs={4}>
				<ProfileCard />
			</Grid>
			<Grid item xs={8}>
				<CurrentFlightMap />
			</Grid>
			{lastFlightLog && currentFlightDetail ? <>
				<Grid item xs={6}>
					<CurrentFlightMainInfo/>
					<CurrentFlightDetail/>
				</Grid>
				<Grid item xs={6}>
					<UserAchievement/>
				</Grid>
			</> : <></>}
			<Grid item xs={12}>
				<RecentFlightsTable />
			</Grid>
		</Grid>
	)
}
