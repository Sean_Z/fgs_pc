import React, {useEffect, useState} from 'react'
import {createStyles, Theme, withStyles} from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TablePagination from '@material-ui/core/TablePagination'
import TableRow from '@material-ui/core/TableRow'
import {RCFMainViewContainer, TableLoadingContainer} from './DashboardStyledComponent'
import {Constant} from '@assets/Constant'
import {FgsService} from '@http/FgsService'
import TableCell from '@material-ui/core/TableCell'
import moment from 'moment'
import {styled} from '@material-ui/core'
import {useAppSelector} from '@store/store'
import {useDispatch} from 'react-redux'
import {fmsActions} from '@store/reducers/FmsReducer'
import {InfiniteFlightLog, InfiniteFlightLogsRes} from '@http/entity/response/InfiniteFlightLogsRes'
import {InfiniteFlightLogsReq} from '@http/entity/request/InfiniteFlightLogReq'
import {theme} from '../../../theme/Theme'
import {grey} from '@mui/material/colors'

const StyledTableCell = withStyles((theme:Theme) =>
	createStyles({
		head: {
			backgroundColor: Constant.Color.themeColor,
			color: theme.palette.common.white,
			borderRadius: 0,
			border: 'none'
		},
		body: {fontSize: 14, borderColor: grey[300]}
	}),
)(TableCell)

const StyledTableRow = styled(TableRow)({
	backgroundColor: theme.palette.action.hover
})

const infiniteFlightLogsColumns: IdLabelPair[] = [
	{id: 'callsign', label: 'Callsign'},
	{id: 'originAirport', label: 'Departure'},
	{id: 'destinationAirport', label: 'Arrival'},
	{id: 'totalTime', label: 'Flight Time'},
	{id: 'aircraftId', label: 'Aircraft'},
	{id: 'liveryId', label: 'Company'},
	{id: 'created', label: 'Flight Date'},
	{id: 'status', label: 'Status'},
]

export default function RecentFlightsTable (): JSX.Element {
	const [page, setPage] = useState<number>(0)
	const [flightData, setFlightData] = useState<InfiniteFlightLogsRes>()
	const handleChangePage = (event: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => setPage(newPage)
	const userName = useAppSelector((state) => state.userReducer.infiniteFlightUserName)
	const currentFlightDetail = useAppSelector((state) => state.fmsReducer.currentFlightDetail)
	const [loading, setLoading] = useState<boolean>(true)
	const dispatch = useDispatch()

	useEffect((): void => {
		setFlightTableData().catch()
	}, [page])

	const setFlightTableData = async (): Promise<void> => {
		const fixedPage = page === 0 ? 1 : page + 1
		const requestBody: InfiniteFlightLogsReq = {
			userName,
			page: fixedPage
		}
		setLoading(true)
		const res = await FgsService.infiniteFlightLogs(requestBody).finally(() => setLoading(false))
		dispatch(fmsActions.setAllFlightLogs(res.data))
		setFlightData(res)
	}

	const filterTableData = (rawData: InfiniteFlightLog[]): InfiniteFlightLog[] => {
		return rawData.filter(element => element.totalTime !== 0)
	}

	const formatFlightDate = (dateString: string): string => {
		return moment(dateString).format('YYYY-MM-DD-HH:MM')
	}

	const formatDestinationAirport = (row: InfiniteFlightLog): string => {
		const {destinationAirport, originAirport} = row
		return destinationAirport ? destinationAirport : originAirport
	}

	const formatFlightStatus = (row: InfiniteFlightLog): string => {
		const {destinationAirport, landingCount} = row
		const conditionExecuting = row.id === currentFlightDetail?.flightId && currentFlightDetail?.speed > 60
		const conditionNormalState = destinationAirport && landingCount > 0 ? 'On Time' : 'Canceled'
		return conditionExecuting ? 'Executing' : conditionNormalState
	}

	const statusCellStyle = (row: InfiniteFlightLog) => {
		const statusColorMap = new Map([
			['Canceled', '#ff1744'],
			['On Time', '#33eb91'],
			['Executing', '#17AEFFFF'],
		])
		const flightStatus = formatFlightStatus(row)
		const statusColor = statusColorMap.get(flightStatus)
		const statusTextSize = '1rem'
		return {
			color: 'white',
			backgroundColor: statusColor,
			fontSize: statusTextSize
		}
	}

	const formatFlightTime = (flightMinutes: number): string => {
		const flightMinutesInt = Math.round(flightMinutes)
		const hr = Math.floor(flightMinutesInt / 60)
		const min = flightMinutesInt % 60
		return `${hr} hrs ${min} mins`
	}

	const FlightLogTable = (): JSX.Element =>
		<Table stickyHeader aria-label='sticky table' title='Recently Flights'>
			<TableHead>
				<TableRow>
					{infiniteFlightLogsColumns.map((column: IdLabelPair): JSX.Element =>
						<StyledTableCell key={column.id}>
							{column.label}
						</StyledTableCell>)}
				</TableRow>
			</TableHead>
			<TableBody>
				{filterTableData(flightData?.data ?? []).map((row): JSX.Element =>
					<StyledTableRow key={row.id}>
						<StyledTableCell align='left'>{row.callsign}</StyledTableCell>
						<StyledTableCell align='left'>{row.originAirport}</StyledTableCell>
						<StyledTableCell align='left'>{formatDestinationAirport(row)}</StyledTableCell>
						<StyledTableCell align='left'>{formatFlightTime(row.totalTime)}</StyledTableCell>
						<StyledTableCell align='left'>{row.aircraftId}</StyledTableCell>
						<StyledTableCell align='left'>{row.liveryId}</StyledTableCell>
						<StyledTableCell align='left'>{formatFlightDate(row.created)}</StyledTableCell>
						<StyledTableCell style={statusCellStyle(row)} align='left'>{formatFlightStatus(row)}</StyledTableCell>
					</StyledTableRow>
				)}
			</TableBody>
		</Table>

	return (
		<>
			<RCFMainViewContainer>
				<TableContainer component={Paper}>
					{loading && <TableLoadingContainer/>}
					<FlightLogTable />
				</TableContainer>
				<TablePagination
					rowsPerPageOptions={[10]}
					component='div'
					count={flightData?.totalPages ?? 0}
					rowsPerPage={10}
					page={page}
			 	onPageChange={handleChangePage}/>
			</RCFMainViewContainer>
		</>
	)
}

interface IdLabelPair {
	readonly id: string
	readonly label: string
}