import React from 'react'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardMedia from '@material-ui/core/CardMedia'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import dashboardImg from '@assets/img/background-img/dashboard-bg.jpg'
import {FootTextView} from '../../homepage/MainInfoStyledComponent'
import {useAppSelector} from '@store/store'

export default function MainInfo(): JSX.Element {
	const username = useAppSelector((state) => state.userReducer.infiniteFlightUserName)
	return (
		<Card>
			<CardActionArea>
				<CardMedia
					component='img'
					alt='Contemplative Reptile'
					height='300'
					image={dashboardImg}
					title='Contemplative Reptile'/>
				<CardContent>
					<Typography gutterBottom variant='h5'>
						Welcome back {username}!
					</Typography>
					<Typography gutterBottom variant='body1'>
						From here you can navigate to file a PiRep<br/>
						check on the flight schedules, weather, plan a
						flight and more!
					</Typography>
					<FootTextView gutterBottom variant='h6'>
						@ Flight Global Studio.
					</FootTextView>
				</CardContent>
			</CardActionArea>
		</Card>
	)
}
