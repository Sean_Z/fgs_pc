import {Constant} from '@assets/Constant'
import {styled} from '@material-ui/core'
import AppBar from '@material-ui/core/AppBar'
import Avatar from '@material-ui/core/Avatar'
import IconButton from '@material-ui/core/IconButton'
import Drawer from '@material-ui/core/Drawer'
import {theme} from '../../../theme/Theme'

const drawerWidth = 240

export const DrawerView = styled('nav')({
	[theme.breakpoints.up ('sm')]: {
		width: drawerWidth,
		flexShrink: 0,
	}
})

export const UIAppBar = styled(AppBar)({
	[theme.breakpoints.up ('sm')]: {
		width: `calc(100% - ${drawerWidth}px)`,
		marginLeft: drawerWidth,
	},
	backgroundColor: Constant.Color.themeColor
})

export const UIAvatar = styled(Avatar)({
	display: 'block',
	margin: 'auto',
	height: '6rem',
	width: '6rem',
	position: 'absolute',
	left: 'calc(50% - 3rem)',
	top: '5.5rem'
})

export const MenuButton = styled(IconButton)({
	marginRight: theme.spacing (2),
	[theme.breakpoints.up ('sm')]: {
		display: 'none',
	}
})

export const DrawerPaperView = styled(Drawer)({
	width: drawerWidth,
	height: '100%',
	borderRight: 'none',
})

export const AccountTextView = styled('div')({
	color: 'white',
	position: 'absolute',
	fontSize: '1rem',
	zIndex: 3,
	marginTop: '-1.5rem'
})

export const ProfileContainer = styled('div')({
	height: '10rem',
	marginTop: '-10px',
	padding: '0',
	backgroundColor: Constant.Color.themeColor
})

export const LogoView = styled('img')({
	zIndex: 5,
	width: '240px',
	height: '64px',
	backgroundColor: Constant.Color.themeColor,
})