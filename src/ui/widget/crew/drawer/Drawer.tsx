import React, {SyntheticEvent, useState} from 'react'
import CssBaseline from '@material-ui/core/CssBaseline'
import Divider from '@material-ui/core/Divider'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import MenuIcon from '@material-ui/icons/Menu'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import {Theme, useTheme} from '@material-ui/core/styles'
import HomeWorkIcon from '@material-ui/icons/HomeWork'
import Avatar from '@material-ui/core/Avatar'
import ExploreIcon from '@material-ui/icons/Explore'
import DashboardIcon from '@material-ui/icons/Dashboard'
import SchoolIcon from '@material-ui/icons/School'
import {
    AccountTextView,
    DrawerPaperView,
    DrawerView,
    LogoView,
    MenuButton,
    ProfileContainer,
    UIAppBar,
    UIAvatar
} from './DrawerStyledComponent'
import {Links} from '@assets/Constant'
import PopupState, {bindMenu, bindTrigger} from 'material-ui-popup-state'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import HomeIcon from '@material-ui/icons/Home'
import {useNavigate} from 'react-router'
import {Variant} from 'material-ui-popup-state/core'
import {MobileFriendlyRounded} from '@material-ui/icons'
import {useAppSelector} from '@store/store'
import EmojiEventsIcon from '@mui/icons-material/EmojiEvents'

export default function Drawers(props: { window: any }): JSX.Element {
    const theme: Theme = useTheme()
    const history = useNavigate()
    const {window} = props.window
    const [mobileOpen, setMobileOpen] = useState<boolean>(false)
    const username = useAppSelector((state) => state.userReducer.infiniteFlightUserName)
    const profilePic: string = username === undefined ? `${Links.fgsStorageBaseUrl}/FGS.png` : `${Links.fgsStorageBaseUrl}/profile/${username}.jpg`
    const email: string = useAppSelector((state) => state.userReducer.email)
    const avatarState: boolean = useAppSelector((state) => state.userReducer.isShowAvatar)

    const handleDrawerToggle = (): void => setMobileOpen(!mobileOpen)

    const logout = (): void => {
        localStorage.removeItem('nodeToken')
        localStorage.removeItem('refreshToken')
        localStorage.removeItem('accessToken')
        history('/login', {state: {hello: 'hello'}})
    }

    // 抽屉组件列表对象数组
    const drawerMap: { component: JSX.Element, itemName: string, method: VoidFunction }[] = [
        {component: <DashboardIcon/>, itemName: 'Dashboard', method: () => history('/crew')},
        {component: <ExploreIcon/>, itemName: 'FGS Status', method: (): void => history('/crew/status')},
        {component: <SchoolIcon/>, itemName: 'FGS Tutorial', method: (): void => history('/crew/tutorial')},
        {component: <HomeIcon/>, itemName: 'HomePage', method: (): void => history('/')},
        {component: <MobileFriendlyRounded/>, itemName: 'About', method: (): void => history('/about')},
        {component: <EmojiEventsIcon/>, itemName: 'Achievement', method: (): void => history('/achievement')},
        {component: <HomeWorkIcon/>, itemName: 'Log out', method: (): void => logout()}
    ]
    const drawerList: JSX.Element[] = drawerMap.map((item) =>
        <ListItem key={item.itemName} button onClick={item.method}>
            <ListItemIcon>{item.component}</ListItemIcon>
            <ListItemText primary={item.itemName}/>
        </ListItem>
    )

    const drawer: JSX.Element =
        <>
            <LogoView src={Links.fgsLogoHorizontal} alt='logo'/>
            <ProfileContainer>
                <UIAvatar src={profilePic} alt='profile'/>
            </ProfileContainer>
            <AccountTextView>{email}</AccountTextView>
            <Divider/>
            <List>{drawerList}</List>
        </>

    const container = window ? () => window().document.body : undefined

    const AvatarPopup = (props: { popupState: PopupStateStruct }): JSX.Element => {
        const {popupState} = props
        return <>
            <Avatar src={profilePic} style={{margin: '0 0 0 auto'}}
                    alt='profile' {...bindTrigger(popupState)}/>
            <Menu {...bindMenu(popupState)}>
                <MenuItem onClick={logout}>Logout</MenuItem>
                <MenuItem onClick={popupState.close}>Profile</MenuItem>
            </Menu>
        </>
    }
    return (
        <>
            <CssBaseline/>
            <UIAppBar position='fixed'>
                <Toolbar>
                    <MenuButton
                        color='inherit'
                        aria-label='open drawer'
                        edge='start'
                        onClick={handleDrawerToggle}>
                        <MenuIcon/>
                    </MenuButton>
                    <Typography variant='h5' noWrap>
                        Crew Center
                    </Typography>
                    <Typography variant={'body1'} style={{position: 'fixed', right: '1rem'}}>
                    </Typography>
                    {avatarState &&
                        <PopupState variant='popover'>
                            {(popupState: PopupStateStruct) => <AvatarPopup popupState={popupState}/>}
                        </PopupState>}
                </Toolbar>
            </UIAppBar>
            <DrawerView aria-label='mailbox folders'>
                <DrawerPaperView
                    container={container}
                    variant='temporary'
                    anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                    open={mobileOpen}
                    onClose={handleDrawerToggle}
                    ModalProps={{keepMounted: true}}>
                    {drawer}
                </DrawerPaperView>
                <DrawerPaperView variant='permanent' open>
                    {drawer}
                </DrawerPaperView>
            </DrawerView>
        </>
    )
}

export interface PopupStateStruct {
    open: (eventOrAnchorEl?: SyntheticEvent<any> | HTMLElement | null) => void
    close: () => void
    toggle: (eventOrAnchorEl?: SyntheticEvent<any> | HTMLElement | null) => void
    onMouseLeave: (event: SyntheticEvent<any>) => void
    setOpen: (
        open: boolean,
        eventOrAnchorEl?: SyntheticEvent<any> | HTMLElement
    ) => void
    isOpen: boolean
    anchorEl: HTMLElement | undefined
    setAnchorEl: (anchorEl: HTMLElement) => any
    setAnchorElUsed: boolean
    disableAutoFocus: boolean
    popupId: string | undefined
    variant: Variant
    _childPopupState: PopupStateStruct | undefined
    _setChildPopupState: (popupState: PopupStateStruct | null | undefined) => void
}