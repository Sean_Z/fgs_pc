import React, {useEffect, useState} from 'react'
import {MapView, StatusChartActionContainer, StatusChartDialogTitleView,} from './LeafletOnlineMapStyledComponent'
import './LeafletOnlineMap.css'
import L, {LatLngExpression, LatLngTuple, LeafletEvent, Marker, MarkerOptions} from 'leaflet'
import 'leaflet-rotatedmarker'
import {Links, OSM_MAP_STYLE} from '@assets/Constant'
import {ServerType} from '../../../../enum/Type'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import StatusCharts from './StatusCharts'
import {FgsService} from '@http/FgsService'
import {CalculateMethod, Tools} from '@core/Tools'
import {useDispatch} from 'react-redux'
import moment from 'moment'
import {uiActions} from '@store/reducers/UIReducer'
import SearchBar from './assistant/SearchBarView'
import LeftDrawer, {AircraftDetailInDrawer} from './assistant/FlightDetailDrawer'
import {osmMapReducerActions} from './assistant/OsmMapReducer'
import {useAppSelector} from '@store/store'
import {fmsActions} from '@store/reducers/FmsReducer'
import {colors} from '@mui/material'
import {AllServerFlightRes, FlightDetail} from '@http/entity/response/AllServerFlightRes'
import {AirwayLineRes} from '@http/entity/response/AirwayLineRes'
import {FetchOnlinePlanRes} from '@http/entity/response/FetchOnlinePlanRes'

let osmMap: L.Map | null = null
let airwayLineLayer = L.polyline([])
let wholeAirwayLineLayer = L.polyline([])
let markerLayer = L.layerGroup([])
const airwayPolyLineOptions = {color: colors.purple[500], weight: 2}
const wholeAirwayPolyLineOptions = {color: 'green', dashArray: '15', weight: 1}

export default function LeafletOnlineMap(): JSX.Element {
    const aircraftIcon = L.icon({
        iconUrl: Links.aircraft_icon,
        iconAnchor: [10, 25]
    })
    // 所有航班数据List
    const [allFlights, setAllFlights] = useState<AllServerFlightRes['data'] | undefined>(undefined)
    // 右侧抽屉
    const [drawer, setDrawer] = useState<boolean>(false)
    // 折线图dialog状态
    const [chartStatus, setChartStatus] = useState<boolean>(false)
    const [menuListData, setMenuListData] = useState<FlightDetail[]>([])
    const [currentServer, setCurrentServer] = useState<ServerType>(ServerType.EXPERT)
    const aircraftDetail = useAppSelector(state => state.osmReducer.aircraftDetail)
    const flightMainInfo = useAppSelector(state => state.osmReducer.mainInfo)
    const dispatch = useDispatch()

    useEffect(() => {
        osmMap = L.map('OMSMAP', {
            center: [31.421563, -121.677433],
            zoom: 6,
            scrollWheelZoom: true
        })
    }, [])

    useEffect(() => {
        initMap()
        return () => {
            osmMap = null
        }
    }, [])

    const fetchFlights = async (serverType: ServerType): Promise<AllServerFlightRes> => {
        const res: AllServerFlightRes = await FgsService.fetchFlight(serverType)
        setMenuListData(res.data)
        return res
    }

    const getAirwayLine = async (flightId: string): Promise<LatLngExpression[]> => {
        const airwayLineList: LatLngExpression[] = []
        dispatch(uiActions.setLoading(true))
        const response = await FgsService.getAirwayLine(flightId)
        dispatch(fmsActions.setCurrentAirway(response))
        const departureTime = `DEPT: ${calculateDeptTime(response)}`
        const arrivalTime = `ARRI: ${calculateDestTime(response)}`
        dispatch(osmMapReducerActions.setMainInfo({departureTime, arrivalTime}))
        for (const airwayPoint of response) {
            airwayLineList.push([airwayPoint.latitude, airwayPoint.longitude])
        }
        return airwayLineList
    }

    const calculateDestTime = (response: AirwayLineRes[]): string => {
        const _arrivalTime = response.at(-1)?.time ?? '--/--'
        return moment(_arrivalTime).format('MM/DD HH:MM')
    }

    const calculateDeptTime = (response: AirwayLineRes[]): string => {
        const departureTime = response.filter(element => element.altitude > response[0].altitude)[0]?.time ?? '--/--'
        return moment(departureTime).format('MM/DD HH:MM')
    }

    const getPlanLine = async (onlinePlanIFS: FetchOnlinePlanRes, airwayPoints: LatLngExpression[]): Promise<LatLngExpression[]> => {
        const PlanLineList: LatLngExpression[] = []
        const {wholeRangePosition} = onlinePlanIFS
        if (JSON.stringify(wholeRangePosition) === '[]') {
            return airwayPoints
        }
        for (const wholeAirwayPoint of wholeRangePosition) {
            PlanLineList.push([wholeAirwayPoint.latitude, wholeAirwayPoint.longitude])
        }
        return PlanLineList
    }

    const setDrawerInfo = (flightBean: AllServerFlightRes['data'][0]): void => {
        const aircraftState: Partial<AircraftDetailInDrawer> = {
            liveryPic: Links.liveryPic(flightBean.aircraftId, flightBean.liveryId),
            altitude: `FL${(flightBean.altitude / 100).toFixed(0)}`,
            speed: `${flightBean.speed.toFixed(0)}kts`,
            aircraftName: flightBean.livery[0]?.aircraftName ?? 'unknown',
            livery: flightBean.livery[0].liveryName,
            verticalSpeed: flightBean.verticalSpeed
        }
        dispatch(osmMapReducerActions.setAircraftDetail(aircraftState))
        dispatch(osmMapReducerActions.setMainInfo({pilotName: flightBean.displayName, callSign: flightBean.callSign}))
    }

    const setRouterInfo = (flightPlanResponse: FetchOnlinePlanRes): void => {
        dispatch(osmMapReducerActions.setAircraftDetail({route: flightPlanResponse.routeInfo}))
        const {departure, arrival} = flightPlanResponse
        dispatch(osmMapReducerActions.setMainInfo({departureICAO: departure, arrivalICAO: arrival}))
    }

    const fetchOnlinePlan = async (flightId: string, aircraftId: string): Promise<FetchOnlinePlanRes> => {
        const flightPlanResponse = await FgsService.fetchOnlinePlan({serverType: currentServer, flightId, aircraftId})
        if (flightPlanResponse.status) Tools.handleException('get flightPlanResponse failed')
        setRouterInfo(flightPlanResponse)
        return flightPlanResponse
    }

    const getAirwayLineLayer = async (map: L.Map, flightPlanResponse: FetchOnlinePlanRes, flightId: string): Promise<L.LatLngExpression[]> => {
        const airwayPoints = await getAirwayLine(flightId)
        const wholeAirwayPoints = await getPlanLine(flightPlanResponse, airwayPoints)
        removePolylineLayer(map)
        airwayLineLayer = L.polyline(airwayPoints, airwayPolyLineOptions)
        wholeAirwayLineLayer = L.polyline(wholeAirwayPoints, wholeAirwayPolyLineOptions)
        return airwayPoints
    }

    const renderTargetMarker = (map: L.Map, flightBean: FlightDetail, airwayPoints: L.LatLngExpression[]): void => {
        const getAlt = (element: L.Layer) => JSON.parse((element as any).options.alt)
        markerLayer.removeLayer(markerLayer.getLayers()
            .find(element => getAlt(element).flightId === flightBean.flightId)!)
        const lastPoint = airwayPoints.at(-1)!
        const targetPoint = CalculateMethod.turfPolylineAndMark(lastPoint, airwayLineLayer)
        L.marker(targetPoint, markerOption(flightBean)).addTo(map)
    }

    const drawPolylineAndPopup = (map: L.Map): void => {
        map.fitBounds(airwayLineLayer.getBounds())
        airwayLineLayer.addTo(map)
        wholeAirwayLineLayer.addTo(map)
    }

    const onMarkerClick = async (map: L.Map | null, e?: LeafletEvent | null, flightObject?: FlightDetail, shouldDraw = true): Promise<void> => {
        if (!map) return
        const flightBean = e ? JSON.parse(e.target.options.alt) : flightObject
        setDrawerInfo(flightBean)
        dispatch(uiActions.setLoading(true))
        const {flightId, aircraftId} = flightBean
        const flightPlanResponse = await fetchOnlinePlan(flightId, aircraftId)
        const airwayPoints = await getAirwayLineLayer(map, flightPlanResponse, flightId)
        renderTargetMarker(map, flightBean, airwayPoints)
        dispatch(uiActions.setLoading(false))
        if (shouldDraw) {
            drawPolylineAndPopup(map)
            setDrawer(true)
        }
    }

    const renderMarkers = async (serverType: ServerType): Promise<void> => {
        setCurrentServer(serverType)
        dispatch(uiActions.setLoading(true))
        const {data: flightBeanList} = await fetchFlights(serverType)
        setAllFlights(flightBeanList)
        renderMarkerIcon(flightBeanList)
        dispatch(uiActions.setLoading(false))
    }

    const markerOption = (flightBeanListElement: FlightDetail): MarkerOptions => {
        const flightIconConfig: MarkerOptions = {}
        flightIconConfig.icon = aircraftIcon
        flightIconConfig.rotationAngle = flightBeanListElement.angle
        flightIconConfig.title = flightBeanListElement.callSign
        flightIconConfig.alt = JSON.stringify(flightBeanListElement)
        return flightIconConfig
    }

    const renderMarkerIcon = (flightBeanList: FlightDetail[]): void => {
        const markerList: Marker[] = []
        for (const flightBeanListElement of flightBeanList) {
            const iconLatitude = flightBeanListElement.position.latitude
            const iconLongitude = flightBeanListElement.position.longitude
            const iconPosition: LatLngTuple = [iconLatitude, iconLongitude]
            const marker = L.marker(iconPosition, markerOption(flightBeanListElement))
            marker.on('click', (e) => onMarkerClick(osmMap, e, undefined))
            markerList.push(marker)
        }
        markerLayer.clearLayers()
        markerLayer = L.layerGroup(markerList)
        osmMap?.removeLayer(markerLayer)
        osmMap?.addLayer(markerLayer)
    }

    const initMap = (): void => {
        if (!osmMap) return
        const tileLayer = L.tileLayer(Links.osmMapUrl, {id: OSM_MAP_STYLE.streets})
        tileLayer.addTo(osmMap)
        renderMarkers(ServerType.EXPERT).catch()
        osmMap.on('click', () => removePolylineLayer(osmMap!))
    }

    const removePolylineLayer = (map: L.Map): void => {
        map.removeLayer(airwayLineLayer)
        map.removeLayer(wholeAirwayLineLayer)
    }

    return <>
        <MapView id={'OMSMAP'}/>
        <Dialog open={chartStatus} maxWidth={'lg'}>
            <StatusChartDialogTitleView>
                Speed & Altitude graph
            </StatusChartDialogTitleView>
            <StatusCharts livery={aircraftDetail.livery ?? ''} callSign={flightMainInfo.callSign ?? ''}/>
            <StatusChartActionContainer>
                <Button
                    onClick={() => setChartStatus(false)}
                    color="primary"
                    autoFocus
                    style={{color: 'white'}}>
                    CLOSE
                </Button>
            </StatusChartActionContainer>
        </Dialog>
        <SearchBar
            allFlights={allFlights}
            menuListData={menuListData}
            osmMap={osmMap}
            onMarkerClick={onMarkerClick}
            currentServer={currentServer} renderMarkers={renderMarkers}/>
        <LeftDrawer
            setDrawer={setDrawer}
            setChartStatus={setChartStatus}
            drawer={drawer}/>
    </>
}
