import L, {LatLngExpression, LatLngTuple, MapOptions, MarkerOptions} from 'leaflet'
import {Constant, Links, OSM_MAP_STYLE} from '@assets/Constant'
import React, {useEffect, useLayoutEffect} from 'react'
import {FgsService} from '@http/FgsService'
import {useDispatch} from 'react-redux'
import {Tools} from '@core/Tools'
import {uiActions} from '@store/reducers/UIReducer'
import {fmsActions} from '@store/reducers/FmsReducer'
import {useAppSelector} from '@store/store'
import {FlightDetail} from '@http/entity/response/AllServerFlightRes'
import {InfiniteFlightLog} from '@http/entity/response/InfiniteFlightLogsRes'
import {InfiniteFlightLogsReq} from '@http/entity/request/InfiniteFlightLogReq'
import {FetchOnlinePlanRes} from '@http/entity/response/FetchOnlinePlanRes'

let osmMap: L.Map | null = null
let airwayLineLayer = L.polyline([])
let wholeAirwayLineLayer = L.polyline([])
const MapId = 'FlightMap'
export default function CurrentFlightMap(): JSX.Element {
    const userName = useAppSelector((state) => state.userReducer.infiniteFlightUserName)
    const aircraftIcon = L.icon({iconUrl: Links.aircraft_icon})
    const dispatch = useDispatch()

    useLayoutEffect(() => {
        const mapOption: MapOptions = {
            zoom: 6,
            center: [31.421563, -121.677433],
            scrollWheelZoom: true,
            touchZoom: false,
            dragging: false,
            zoomControl: false
        }
        osmMap = L.map(MapId, mapOption)
        return () => {
            osmMap = null
        }
    }, [])
    useLayoutEffect(() => initMap())
    useEffect(() => {
        uiActions.setLoading(true)
        renderMyFlightMarker().finally(() => uiActions.setLoading(false))
    }, [])

    const initMap = (): void => {
        L.tileLayer(Links.osmMapUrl, {
            attribution: '',
            id: OSM_MAP_STYLE.light
        }).addTo(osmMap!)
    }

    const renderMyFlightMarker = async (): Promise<FlightDetail | null> => {
        const _myLastFlight = await myLastFlight()
        const serverType = Constant.ServerMap.get(_myLastFlight.server) ?? 0
        const myFlight = await whereIsMyFlight(_myLastFlight)
        if (!myFlight) return null
        const flightIconConfig: MarkerOptions = {}
        flightIconConfig.icon = aircraftIcon
        flightIconConfig.rotationAngle = myFlight.angle
        flightIconConfig.title = myFlight.callSign
        flightIconConfig.alt = JSON.stringify(myFlight)
        const iconLatitude = myFlight.position.latitude
        const iconLongitude = myFlight.position.longitude
        const iconPosition: LatLngTuple = [iconLatitude, iconLongitude]
        L.marker(iconPosition, flightIconConfig).addTo(osmMap!)
        renderAirwayLine(serverType, myFlight).catch(() => Tools.handleException('render my flight failed'))
        return myFlight
    }

    const myLastFlight = async (): Promise<InfiniteFlightLog> => {
        const req: InfiniteFlightLogsReq = {page: 1, userName}
        const response = await FgsService.infiniteFlightLogs(req)
        const [firstLog] = response.data
        return firstLog
    }

    /**
     * get my current flight information
     * @param myLastFlight last flight from log interface
     * this log also is current flight
     */
    const whereIsMyFlight = async (myLastFlight: InfiniteFlightLog): Promise<FlightDetail | void> => {
        const {id: flightId, server} = myLastFlight
        const targetFlightGroup = await FgsService.fetchFlight(Constant.ServerMap.get(server) ?? 0)
        const myFlight = targetFlightGroup.data.find(element => element.flightId === flightId)
        dispatch(fmsActions.setLastFlightLog(myLastFlight))
        if (!myFlight) {Tools.handleException('target not find', false);return}
        pinToMyFlight(myFlight)
        dispatch(fmsActions.setCurrentFlight(myFlight))
        return myFlight
    }

    /**
     * get flight plan points
     * @param fetchOnlinePlanResponse
     * @param flightId
     */
    const getPlanLine = async (fetchOnlinePlanResponse: FetchOnlinePlanRes, flightId: string): Promise<LatLngExpression[]> => {
        const PlanLineList: LatLngExpression[] = []
        const airwayPoints = await getAirwayLine(flightId)
        const wholeRangePosition = fetchOnlinePlanResponse.wholeRangePosition ?? []
        if (wholeRangePosition.length === 0) return airwayPoints
        for (const wholeAirwayPoint of wholeRangePosition) {
            PlanLineList.push([wholeAirwayPoint.latitude, wholeAirwayPoint.longitude])
        }
        return PlanLineList
    }

    /**
     * get airway points which already flew by my current flight
     * @param flightId flight id
     */
    const getAirwayLine = async (flightId: string): Promise<LatLngExpression[]> => {
        const airwayLineList: LatLngExpression[] = []
        const response = await FgsService.getAirwayLine(flightId)
        dispatch(fmsActions.setCurrentAirway(response))
        for (const airwayPoint of response) {
            airwayLineList.push([airwayPoint.latitude, airwayPoint.longitude])
        }
        return airwayLineList
    }

    const pinToMyFlight = (myFlight: FlightDetail): void => {
        const {latitude, longitude} = myFlight.position
        const myPosition: LatLngExpression = {lat: latitude, lng: longitude}
        osmMap?.flyTo(myPosition)
    }

    const renderAirwayLine = async (serverType: number, myFlight: FlightDetail): Promise<void> => {
        if (!osmMap) return
        const {flightId, aircraftId} = myFlight
        const flightPlanResponse = await FgsService.fetchOnlinePlan({serverType, flightId, aircraftId})
        dispatch(fmsActions.setFlightRoute(flightPlanResponse))
        const wholeAirwayPoints = await getPlanLine(flightPlanResponse, flightId)
        const airwayPoints = await getAirwayLine(flightId)
        const airwayPolyLineOptions = {color: 'red', weight: 2}
        const wholeAirwayPolyLineOptions = {color: 'green', dashArray: '15', weight: 1}
        airwayLineLayer = L.polyline(airwayPoints, airwayPolyLineOptions)
        wholeAirwayLineLayer = L.polyline(wholeAirwayPoints, wholeAirwayPolyLineOptions)
        airwayLineLayer.addTo(osmMap)
        wholeAirwayLineLayer.addTo(osmMap)
        osmMap.fitBounds(airwayLineLayer.getBounds())
    }

    return <div id={MapId} style={{width: '100%', height: '100%'}}/>
}
