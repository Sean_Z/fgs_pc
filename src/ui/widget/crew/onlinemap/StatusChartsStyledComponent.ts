import {styled} from '@material-ui/core'

export const StateChartContainer = styled('div')({
	width: 800,
	height: 400,
	margin: 'auto',
	display: 'contents',
	backgroundColor: '#2c343c'
})