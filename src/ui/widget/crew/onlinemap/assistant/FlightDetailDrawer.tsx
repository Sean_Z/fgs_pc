import Drawer from '@material-ui/core/Drawer'
import {
    AirlineIcon,
    ArrivalIcon,
    DeptIcon,
    FlightDetailCard,
    FlightDetailTitleView,
    FlightDetailView,
    RouteItemContainer,
    UICardContent
} from '../LeafletOnlineMapStyledComponent'
import CardMedia from '@material-ui/core/CardMedia'
import {Links} from '@assets/Constant'
import Divider from '@material-ui/core/Divider'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import PermIdentityIcon from '@material-ui/icons/PermIdentity'
import LocalAirportIcon from '@material-ui/icons/LocalAirport'
import {Tools} from '@core/Tools'
import CardActionArea from '@material-ui/core/CardActionArea'
import TimelineIcon from '@material-ui/icons/Timeline'
import FilterHdrIcon from '@material-ui/icons/FilterHdr'
import SpeedIcon from '@material-ui/icons/Speed'
import MapIcon from '@material-ui/icons/Map'
import CardActions from '@material-ui/core/CardActions'
import Button from '@material-ui/core/Button'
import React, {useMemo} from 'react'
import copy from 'copy-to-clipboard'
import {useDispatch} from 'react-redux'
import {osmMapReducerActions} from './OsmMapReducer'
import {useAppSelector} from '@store/store'
import {changeAircraftStatus} from './LeafletMapLogic'
import {useSuccessSnackBar} from '../../../public/UISnackbar'

export interface LeftDrawerProps {
    readonly drawer: boolean
    readonly setDrawer: (state: boolean) => void
    readonly setChartStatus: (state: boolean) => void
}

export interface AircraftDetailInDrawer {
    readonly aircraftName: string
    readonly livery: string
    readonly altitude: string
    readonly speed: string
    readonly route: string
    readonly liveryPic: string
    readonly verticalSpeed: number
}

export interface FlightDetailDrawerMain {
    readonly pilotName: string
    readonly aircraftStatus: JSX.Element
    readonly departureTime: string
    readonly departureICAO: string
    readonly arrivalICAO: string
    readonly arrivalTime: string
    readonly callSign: string
}

export default function LeftDrawer(props: LeftDrawerProps): JSX.Element {
    const {drawer, setDrawer, setChartStatus} = props
    const flightMainInfo = useAppSelector(state => state.osmReducer.mainInfo)
    const {aircraftName, livery, altitude, speed, route, liveryPic, verticalSpeed} = useAppSelector(state => state.osmReducer.aircraftDetail)
    const cachedLiveryPic = useMemo(() => liveryPic, [liveryPic])
    const showSuccessSnackBar = useSuccessSnackBar()
    const dispatch = useDispatch()

    const copyRoute = (): void => {
        showSuccessSnackBar('copied!')
        copy(route ?? '')
    }


    return <Drawer anchor={'right'} open={drawer} onClose={() => setDrawer(!drawer)}
                   variant="persistent">
        <FlightDetailView>
            <FlightDetailCard>
                <CardMedia
                    style={{height: '15rem'}}
                    image={cachedLiveryPic}
                    title="Aircraft Livery"
                    component="img"
                    onError={() => dispatch(osmMapReducerActions.setAircraftDetail({liveryPic: Links.unknownAircraft}))}/>
                <UICardContent>
                    <FlightDetailTitleView>
                        <div>{flightMainInfo.pilotName}</div>
                        {changeAircraftStatus({speed, altitude, verticalSpeed})}
                    </FlightDetailTitleView>
                    <Divider/>
                    <List>
                        <ListItem>
                            <ListItemText secondary={flightMainInfo.departureTime}>
                                <DeptIcon/>{flightMainInfo.departureICAO}
                            </ListItemText>
                            <ListItemText secondary={flightMainInfo.arrivalTime}>
                                <ArrivalIcon/>{flightMainInfo.arrivalICAO}
                            </ListItemText>
                        </ListItem>
                        <ListItem>
                            <ListItemIcon><PermIdentityIcon/></ListItemIcon>
                            <ListItemText primary={`CallSign:   ${flightMainInfo.callSign}`}/>
                        </ListItem>
                        <ListItem>
                            <ListItemIcon><LocalAirportIcon/></ListItemIcon>
                            <ListItemText primary={aircraftName} secondary={livery}/>
                            <AirlineIcon style={{zoom: 0.4}}><img
                                src={Tools.getAirlineIcon(livery ?? '')} alt={''}/></AirlineIcon>
                        </ListItem>
                        <CardActionArea onClick={() => setChartStatus(true)}>
                            <ListItem>
                                <ListItemIcon><TimelineIcon/></ListItemIcon>
                                <ListItemText primary={'Check charts'}/>
                            </ListItem>
                        </CardActionArea>
                        <ListItem>
                            <ListItemIcon><FilterHdrIcon/></ListItemIcon>
                            <ListItemText primary={`Altitude:   ${altitude}`}/>
                        </ListItem>
                        <ListItem>
                            <ListItemIcon><SpeedIcon/></ListItemIcon>
                            <ListItemText primary={`Speed:   ${speed}`}/>
                        </ListItem>
                        <ListItem>
                            <ListItemIcon><MapIcon/></ListItemIcon>
                            <RouteItemContainer primary={`Route:   ${route}`}/>
                        </ListItem>
                    </List>
                </UICardContent>
                <CardActions>
                    {route !== 'Loading' &&
                        <Button
                            variant="text"
                            size="small"
                            color="primary"
                            onClick={copyRoute}>
                            Copy Route
                        </Button>}
                    <Button
                        size="small"
                        color="primary"
                        onClick={() => setDrawer(!drawer)}>
                        Close
                    </Button>
                </CardActions>
            </FlightDetailCard>
        </FlightDetailView>
    </Drawer>
}