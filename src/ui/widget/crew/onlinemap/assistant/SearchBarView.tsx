import {ServerType} from '../../../../../enum/Type'
import {
    MapTypeMenuIcon,
    MenuButton,
    SearchBarContainer,
    SearchBarView,
    SearchInputView,
    ServerPaperView
} from '../LeafletOnlineMapStyledComponent'
import {Stars, TrendingUp} from '@material-ui/icons'
import SchoolIcon from '@material-ui/icons/School'
import {PopupStateStruct} from '../../drawer/Drawer'
import PopupState, {bindMenu, bindTrigger} from 'material-ui-popup-state'
import MenuIcon from '@material-ui/icons/Menu'
import Typography from '@material-ui/core/Typography'
import {Constant} from '@assets/Constant'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import IconButton from '@material-ui/core/IconButton'
import SearchIcon from '@material-ui/icons/Search'
import React from 'react'
import L, {LeafletEvent} from 'leaflet'
import {AllServerFlightRes, FlightDetail} from '@http/entity/response/AllServerFlightRes'
import {useErrorSnackBar} from '../../../public/UISnackbar'

export interface SearchBarProps {
    readonly renderMarkers: (serverType: ServerType) => Promise<void>
    readonly currentServer: ServerType
    readonly menuListData: FlightDetail[]
    readonly allFlights: AllServerFlightRes['data'] | undefined
    readonly osmMap: L.Map | null
    readonly onMarkerClick: (map: L.Map | null, e?: LeafletEvent | null, flightObject?: FlightDetail) => Promise<void>
}
let targetPilotName: string
/**
 * 搜搜条控件 包含搜索框 地图样式控件 服务器选择控件
 * @constructor
 */
export default function SearchBar(props: SearchBarProps): JSX.Element {
    const {renderMarkers, currentServer, menuListData, allFlights, onMarkerClick, osmMap} = props
    const showErrorSnackBar = useErrorSnackBar()
    const serverTypeMenuList = [
        {
            method: () => renderMarkers(ServerType.EXPERT),
            icon: <MapTypeMenuIcon><Stars/></MapTypeMenuIcon>,
            label: 'Expert Server'
        },
        {
            method: () => renderMarkers(ServerType.TRAINING),
            icon: <MapTypeMenuIcon><SchoolIcon/></MapTypeMenuIcon>,
            label: 'Training Server'
        },
        {
            method: () => renderMarkers(ServerType.CASUAL),
            icon: <MapTypeMenuIcon><TrendingUp/></MapTypeMenuIcon>,
            label: 'Casual Server'
        }
    ]

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
        targetPilotName = event.target.value
    }

    const findTarget = async (map: L.Map | null, targetUserName: string): Promise<void> => {
        const temp = allFlights!.filter((obj) => obj.displayName === targetUserName)
        if (map !== null && temp.length !== 0) {
            const flightBean = temp[0]
            await onMarkerClick(osmMap, null, flightBean)
        } else {
            showErrorSnackBar('not found target!')
        }
    }

    const ServerPopup = (props: {popupState: PopupStateStruct}): JSX.Element => {
        const {popupState} = props
        const action = (item: (typeof serverTypeMenuList)[0]) => {
            popupState.close()
            item.method().then()
        }
        return <>
            <MenuButton {...bindTrigger(popupState)} aria-label='menu'>
                <ServerPaperView>
                    <MenuIcon/>
                    <Typography>
                        {Constant.ServerNameMap.get(currentServer)}
                    </Typography>
                </ServerPaperView>
            </MenuButton>
            <Menu {...bindMenu(popupState)}>
                {serverTypeMenuList.map((item) =>
                    <MenuItem
                        key={item.label}
                        onClick={() => action(item)}>
                        {item.icon}{item.label}
                    </MenuItem>)}
            </Menu>
        </>
    }
    return <SearchBarView>
        <SearchBarContainer>
            <PopupState variant='popover' popupId='demo-popup-menu'>
                {(popupState) => <ServerPopup popupState={popupState}/>}
            </PopupState>
            <SearchInputView
                onChange={handleInputChange}
                value={targetPilotName}
                type={'text'}
                placeholder='Search Flight'
                inputProps={{'aria-label': 'search google maps'}}/>
            <PopupState variant='popover' popupId='demo-popup-menu'>
                {(popupState) =>
                    <>
                        <IconButton {...bindTrigger(popupState)}>
                            <SearchIcon/>
                        </IconButton>
                        <Menu {...bindMenu(popupState)}>
                            {
                                menuListData?.filter((item) => item.displayName.includes(!targetPilotName ? '' : targetPilotName)).map((data) => {
                                    return <MenuItem key={data.flightId} onClick={() => {
                                        popupState.close()
                                        findTarget(osmMap, data.displayName).then()
                                    }}>{data.callSign}</MenuItem>
                                })
                            }
                        </Menu>
                    </>
                }
            </PopupState>
        </SearchBarContainer>
    </SearchBarView>
}