import {StatusColor} from '../../../../../enum/Color'
import React from 'react'

export const changeAircraftStatus = (data: AircraftStatusIFS): JSX.Element => {
    const {speed, verticalSpeed, altitude} = data
    const _speed = Number(speed)
    const _verticalSpeed = Number(verticalSpeed)
    const _altitude = Number(altitude)
    const conditionGround = Math.abs(_verticalSpeed) <= 400 && Number(_speed.toFixed(0)) === 0
    const conditionClimb = _verticalSpeed >= 1000 && _altitude >= 10000
    const conditionTakeOff = _verticalSpeed >= 1000 && _altitude <= 10000
    const conditionLeveling = Math.abs(_verticalSpeed) <= 400 && _altitude >= 10000 && _altitude <= 20000 && _speed >= 100
    const conditionTaxiing = _speed <= 100
    const conditionCruising = Math.abs(_verticalSpeed) <= 400 && _altitude >= 20000
    const conditionDescending = _verticalSpeed <= -400
    const conditionApproaching = _verticalSpeed <= -400 && _altitude <= 10000
    const conditionMap = new Map<boolean, JSX.Element>()
    conditionMap.set(conditionGround, <p style={{color: StatusColor.GROUND}}>Parking</p>)
    conditionMap.set(conditionClimb, <p style={{color: StatusColor.CLIMBING}}>Climbing</p>)
    conditionMap.set(conditionTakeOff, <p style={{color: StatusColor.TAKEOFF}}>Taking off</p>)
    conditionMap.set(conditionLeveling, <p style={{color: StatusColor.LEVELING}}>Leveling off</p>)
    conditionMap.set(conditionTaxiing, <p style={{color: StatusColor.LEVELING}}>Taxiing</p>)
    conditionMap.set(conditionCruising, <p style={{color: StatusColor.CRUISE}}>Cruising</p>)
    conditionMap.set(conditionDescending, <p style={{color: StatusColor.DESCENDING}}>Descending</p>)
    conditionMap.set(conditionApproaching, <p style={{color: StatusColor.APPROACH}}>Approaching</p>)
    return conditionMap.get(true) ?? <p style={{color: StatusColor.APPROACH}}>Approaching</p>
}

export interface AircraftStatusIFS {
    verticalSpeed: number
    altitude: string
    speed: string
}