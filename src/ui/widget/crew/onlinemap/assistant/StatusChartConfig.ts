import * as echarts from 'echarts'
import {EChartsOption} from 'echarts'

export const option = (livery: string, callSign: string, timeList: string[], altitudeList: number[], speedList: number[]): EChartsOption => {
	return  {
		title: {
			padding: [5, 10],
			text:  `${livery} ${callSign}`,
			textStyle: {
				color: 'white'
			}
		},
		backgroundColor: '#2c343c',
		textStyle: {
			color: 'white'
		},
		tooltip: {
			trigger: 'axis'
		},
		legend: {
			padding: [5, 10],
			data: ['Altitude', 'Speed'],
			left:'center',
			textStyle: {
				color: 'white'
			}
		},
		grid: {
			left: '3%',
			right: '4%',
			bottom: '3%',
			containLabel: true
		},
		toolbox: {
			emphasis:{
				iconStyle:{
					borderColor: 'white'
				}
			},
			feature: {
				saveAsImage: {},
				iconStyle:{
					borderColor: 'white'
				}
			}
		},
		xAxis: [{
			type: 'category',
			boundaryGap: false,
			data: timeList
		}],
		yAxis: [
			{
				splitLine: {
					show: false
				},
				type: 'value',
				axisLabel:{formatter:'{value} ft'}
			},
			{
				type: 'value',
				axisLabel: {formatter:'{value} kts'}
			}],
		series: [
			{
				symbol: 'none',
				name: 'Altitude',
				type: 'line',
				stack: 'Altitude',
				data: altitudeList,
				yAxisIndex: 0,
				lineStyle: {
					width: 2,
					type: 'solid',
					color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
						offset: 0,
						color: '#E65100'
					}, {
						offset: 1,
						color: '#FFF9C4'
					}]),
				},
				itemStyle: {color: '#FFF9C4'}
			},
			{
				symbol: 'none',
				name: 'Speed',
				type: 'line',
				stack: 'Speed',
				data: speedList,
				yAxisIndex: 1,
				lineStyle: {
					width: 2,
					type: 'solid',
					color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
						offset: 0,
						color: '#0277BD'
					}, {
						offset: 1,
						color: '#90CAF9'
					}]),
				},
				itemStyle: {color: '#90CAF9'}
			}
		]
	}
}