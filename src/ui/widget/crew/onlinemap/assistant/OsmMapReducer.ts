import {createSlice, PayloadAction} from '@reduxjs/toolkit'
import {AircraftDetailInDrawer, FlightDetailDrawerMain} from './FlightDetailDrawer'

const osmMapState = {
    pilotName: '',
    mainInfo: {} as FlightDetailDrawerMain,
    aircraftDetail: {} as AircraftDetailInDrawer
}

export const osmMapReducerSlice = createSlice({
    name: 'osmMapReducerReducer',
    initialState: osmMapState,
    reducers: {
        setPilotName(state = osmMapState, action: PayloadAction<string>) {
            state.pilotName = action.payload
        },
        setMainInfo(state = osmMapState, action: PayloadAction<Partial<FlightDetailDrawerMain>>) {
            state.mainInfo = {...state.mainInfo, ...action.payload}
        },
        setAircraftDetail(state = osmMapState, action: PayloadAction<Partial<AircraftDetailInDrawer>>) {
            state.aircraftDetail = {...state.aircraftDetail, ...action.payload}
        }
    }
})
export const osmMapReducerActions = osmMapReducerSlice.actions