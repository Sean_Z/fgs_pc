import {styled} from '@material-ui/core'
import ListItemText from '@material-ui/core/ListItemText'
import DialogActions from '@material-ui/core/DialogActions'
import DialogTitle from '@material-ui/core/DialogTitle'
import FlightTakeoffIcon from '@material-ui/icons/FlightTakeoff'
import FlightLandIcon from '@material-ui/icons/FlightLand'
import Paper from '@material-ui/core/Paper'
import Card from '@material-ui/core/Card'
import InputBase from '@material-ui/core/InputBase'
import IconButton from '@material-ui/core/IconButton'
import CardContent from '@material-ui/core/CardContent'
import {theme} from '../../../theme/Theme'

export const MapView = styled('div')({
	width: '100%',
	height: '100%',
	position: 'absolute',
	display: 'flex'
})

export const SearchBarView = styled('div')({
	width: '20rem',
	zIndex: 400,
	position: 'absolute',
	right: '1rem',
	top: '5rem'
})

export const ServerPaperView = styled('div')({
	display: 'flex',
	alignItems: 'center',
	justifyContent: 'center'
})

export const SearchBarContainer = styled(Paper)({
	padding: '2px 4px',
	display: 'flex',
	alignItems: 'center'
})

export const MapTypeMenuIcon = styled('div')({
	marginRight: '1rem'
})

export const AirlineIcon = styled(ListItemText)({
	height: '1rem',
	display: 'inline-block',
	marginRight: '1rem'
})

export const StatusChartActionContainer = styled(DialogActions)({
	backgroundColor: '#2c343c',
})

export const StatusChartDialogTitleView = styled(DialogTitle)({
	background: '#2c343c',
	color: 'white',
	textAlign: 'center'
})

export const RouteItemContainer = styled(ListItemText)({
	display: '-webkit-box',
	'-webkit-box-orient': 'vertical',
	'-webkit-line-clamp': 3,
	overflow: 'hidden'
})

export const DeptIcon = styled(FlightTakeoffIcon)({
	margin: 'auto 1rem auto 0',
})

export const ArrivalIcon = styled(FlightLandIcon)({
	marginLeft: ' 2rem',
	marginRight: '1rem'
})

export const FlightDetailCard = styled(Card)({
	height: '100%',
	overflow: 'auto'
})

export const FlightDetailView = styled('div')({
	width: '25rem',
	height: '100%'
})

export const SearchInputView = styled(InputBase)({
	marginLeft: theme.spacing(1),
	flex: 1
})

export const MenuButton = styled(IconButton)({
	padding: 10
})

export const FlightDetailTitleView = styled('div')({
	display: 'flex',
	flexDirection: 'row',
	alignItems: 'center',
	justifyContent: 'space-between',
	fontSize: 20
})

export const UICardContent = styled(CardContent)({
	paddingTop: 0,
	paddingBottom: 0
})