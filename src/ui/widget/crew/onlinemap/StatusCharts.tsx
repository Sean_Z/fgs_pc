import {StateChartContainer} from './StatusChartsStyledComponent'
import {CalculateMethod, Tools} from '@core/Tools'
import {option} from './assistant/StatusChartConfig'
import React, {useLayoutEffect} from 'react'
import {useAppSelector} from '@store/store'

interface Props {
	livery: string,
	callSign: string
}

export default function StatusCharts(props: Props): JSX.Element {
	const currentAirwayPoints = useAppSelector((state) => state.fmsReducer.currentAirway)
	useLayoutEffect(() => {setChart().catch()}, [currentAirwayPoints])
	const {livery, callSign} = props
	const setChart = async (): Promise<void> => {
		const altitudeList: number[] = []
		const speedList: number[] = []
		const timeList: string[] = []
		for (let i = 0; i < currentAirwayPoints.length - 1; i++) {
			altitudeList.push(currentAirwayPoints[i].altitude)
			speedList.push(currentAirwayPoints[i].groundSpeed)
			const timeBean = CalculateMethod.convertUTCDateToLocalDate(new Date(currentAirwayPoints[i].time))
			timeList.push(`${timeBean.hour}:${timeBean.minute}`)
		}
		const config = option(livery, callSign, timeList, altitudeList, speedList)
		Tools.buildEcharts('statusCharts', config)
	}
	return <StateChartContainer id={'statusCharts'} />
}