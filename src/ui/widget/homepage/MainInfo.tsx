import Button from '@material-ui/core/Button'
import React, {useState} from 'react'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogActions from '@material-ui/core/DialogActions'
import {
	ActionButtonContainer,
	CenterContainer,
	CrewCenterActionButton,
	LearnActionButton,
	TitleTextView,
	TitleTextViewContainer
} from './MainInfoStyledComponent'
import Typography from '@material-ui/core/Typography'
import {NavigateFunction, useNavigate} from 'react-router'

/**
 * @method 此组件为homepage页面主干信息
 * @Author Sean_Zhang
 */
export default function MainInfo(): JSX.Element {
	const history: NavigateFunction = useNavigate()
	const [dialogOpen, setDialogOpen] = useState<boolean>(false)

	const PresentDialog = (): JSX.Element => {
		return <Dialog
			open={dialogOpen}
			onClose={handleCloseDialog}
			aria-labelledby='alert-dialog-title'
			aria-describedby='alert-dialog-description'>
			<DialogTitle id='alert-dialog-title'>{'Words from our president Paul Chen!'}</DialogTitle>
			<DialogContent>
				<DialogContentText id='alert-dialog-description'>
                    Actually FGS created based on infinite flight which is a flight simulator game,
                    We knew each other because of IF,but we stay here because of our shareable life.
                    We love playing with each other but we love sharing our life here much more.
                    Our top goal is transforming our property from game to life, from online to real life!
				</DialogContentText>
			</DialogContent>
			<DialogActions>
				<Button onClick={handleCloseDialog} color='primary'>
                    Disagree
				</Button>
				<Button onClick={toAbout} color='primary' autoFocus>
                    Agree
				</Button>
			</DialogActions>
		</Dialog>
	}

	const handleCloseDialog = (): void => {
		localStorage.setItem('isAgreed', 'false')
		setDialogOpen(false)
	}

	const handleOpen = (): void => {
		const isAgree: string | null = localStorage.getItem('isAgreed')
		isAgree === 'true' ? toAbout() : setDialogOpen(true)
	}

	const toAbout = (): void => {
		localStorage.setItem('isAgreed', 'true')
		history('/about')
	}

	return <>
		<TitleTextViewContainer>
			<TitleTextView>
                Flight Global Studio
			</TitleTextView>
		</TitleTextViewContainer>
		<CenterContainer>
			<div style={{flex: 1, color: 'antiquewhite'}}>
				<Typography variant='h6'>
                    We knew each other because of IF,but we stay here because of our shareable life.
				</Typography>
				<Typography variant='h6'>
                    We love playing with each other but we love sharing our life here much more.
				</Typography>
			</div>
			<ActionButtonContainer>
				<LearnActionButton variant='contained'
					disableElevation
					onClick={handleOpen}>
                    Learn more !
				</LearnActionButton>
				<CrewCenterActionButton
					variant='contained'
					disableElevation
					onClick={() => history('/crew')}>
                    Crew Center
				</CrewCenterActionButton>
			</ActionButtonContainer>
		</CenterContainer>

		<PresentDialog/>
	</>
}
