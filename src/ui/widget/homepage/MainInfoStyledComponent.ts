import {Constant} from '@assets/Constant'
import {styled} from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'

export const TitleTextView = styled(Typography)({
	fontWeight: 500,
	fontSize: 48,
	fontFamily: '-apple-system'
})

export const TitleTextViewContainer = styled('div')({
	height: '20vh',
	display: 'flex',
	left: 0,
	right: 0,
	marginTop: '4rem',
	justifyContent: 'center',
	alignItems: 'center',
	color: 'white'
})

export const ActionButtonContainer = styled('div')({
	flex: 1,
	textAlign: 'center',
	left: 0,
	right: 0,
})

export const LearnActionButton = styled(Button)({
	background: Constant.Color.orange,
	marginRight: '2%',
	marginLeft: '2%',
	width: '12rem',
	height: '2.5rem',
	color: 'white',
	'&:hover': {
		background: Constant.Color.green
	}
})

export const CrewCenterActionButton = styled(Button)({
	marginRight: '2%',
	marginLeft: '2%',
	width: '12rem',
	height: '2.5rem',
	background: '#2d7ff1',
	color: 'white',
	'&:hover': {
		background: '#653cc4'
	}
})

export const CenterContainer = styled('div')({
	padding: '1rem',
	flexDirection: 'column',
	textAlign: 'center',
	width: '80%',
	height: '500px',
	margin: '0 auto',
	display: 'flex',
	justifyContent: 'space-between',
})

export const FootTextView = styled(Typography)({
	float: 'right',
	marginTop: '-2.4rem'
})