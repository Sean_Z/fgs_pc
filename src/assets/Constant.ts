/**
 * @class 此文件为React层各类静态数据原始记录导出层, 同时判断如果用户名找不到就重新登录
 * @author Sean_Zhang
 */
import {AgesList} from '../ui/widget/application/ApplicationForm'

export class Constant {
	// 年龄常量值 usage ===> ApplicationForm Component
	public static readonly AGES: AgesList[] = [{value: '18-20', label: '18-20',}, {value: '21-25', label: '21-25'},
		{value: '26-30', label: '26-30',}, {value: '30+', label: '30+'}]
	// 所有颜色常量
	public static readonly Color = {
		// 主题色
		themeColor: '#4F738B',
		// FGS标准绿色
		green: '#4caf50',
		// FGS 标准橘色
		orange: 'coral',
		// 航迹线亮色
		airwayColorLight: '#FBFF00',
		// 航迹线暗色
		airwayColorDark: '#4F738B'
	}
	// Github登录信息
	public static readonly GITHUB_USER = {
		clientId: 'afb10cdb1725ef411c22',
		clientSecrets: '532e4fc6ab68195e4fd3b07299e0eabe82d73d85',
		redirectUri: 'https://flightglobalstu.xyz',
		requestCodeUri: 'https://github.com/login/oauth/authorize?'
	}

	public static readonly ServerMap: Map<string, number> = new Map([
		['Expert Server', 0],
		['Training Server', 1],
		['Casual Server', 2],
	])

	public static readonly ServerNameMap: Map<number, string> = new Map([
		[0, 'ES'],
		[1, 'TS'],
		[2, 'CS'],
	])
}

export enum OSM_MAP_STYLE {
	streets =  'mapbox/streets-v11',
	light = 'mapbox/light-v10',
}

export class Links {
	// FGS 主页
	public static readonly fgsLink: string = 'https://flightglobalstu.xyz'
	public static readonly liveryPic = (aircraftId: string, liveryId: string): string => {
		return `https://www.infinitex.app/aircraft-livery/${aircraftId}/${liveryId}.webp`
	}
	// FGS存储桶url
	public static readonly fgsStorageBaseUrl: string = 'https://fgs-1300812707.cos.ap-shanghai.myqcloud.com/'
	// 在线地图飞机图标svg
	public static readonly aircraft_icon: string = 'https://fgs-1300812707.cos.ap-shanghai.myqcloud.com/icon/AircraftIcon.svg'
	// FGS横的logo
	public static readonly fgsLogoHorizontal: string = 'https://s1.ax1x.com/2020/04/23/JdmlV0.png'
	// 航司图标
	public static readonly airline_icon: string = 'https://www.flightradar24.com/static/images/data/operators/'
	// infinite flight blog iframe地址
	public static readonly ifBlog: string = 'https://infiniteflight.com/blog/'
	public static readonly ifAtc: string = 'https://infiniteflight.com/guide/atc-manual'
	public static readonly ifPilot: string = 'https://infiniteflight.com/guide/flying-guide'
	// 位置机型图片
	public static readonly unknownAircraft: string = 'https://cdn.liveflightapp.com/aircraft-images/default.jpg'
	// OSM Map 初始化地址
	public static readonly osmMapUrl: string = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1Ijoic2VhbjE5OTciLCJhIjoiY2tudmhudDBqMG1zbDJvcm1tM2VhcGJ2bSJ9.N3MiY_b4mfA98t_XLfFvGw'
}