/**
 * @author Sean_Zhang
 * @date 2021/2/22 5:29 pm
 * @description 各种类型枚举值
 */

/**
 * @author Sean_Zhang
 * @date 2021/2/22 5:27 pm
 * @description 登录类型
 */
enum LoginType {
    // 账号密码登录
    ACCOUNT = 0,
    // 短信登录
    SMS = 1,
    // 邮箱登录
    MAIL= 2,
    // 微信的登录
    WECHAT = 3
}

enum ServerType {
    // ES
    EXPERT = 0,
    TRAINING = 1,
    CASUAL = 2
}
export {LoginType, ServerType}
