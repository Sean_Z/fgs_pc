/**
 * @author Sean_Zhang
 * @date 2021/2/22 5:32 pm
 * @description 各种颜色枚举值
 */

/**
 * @author Sean_Zhang
 * @date 2021/2/9 3:31 下午
 * @description 飞行状态颜色
 */
enum StatusColor {
    GROUND = '#FB8C00',
    TAKEOFF = '#00E676',
    CLIMBING = '#00E676',
    LEVELING = '#40C4FF',
    CRUISE = '#40C4FF',
    DESCENDING = '#FF7043',
    APPROACH = '#E64A19'
}

export {StatusColor}
