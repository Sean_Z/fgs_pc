import React, {lazy, Suspense} from 'react'
import {Route, Routes} from 'react-router-dom'
import CircularProgress from '@material-ui/core/CircularProgress'
import Backdrop from '@material-ui/core/Backdrop'

const AsyncHome = lazy(() => import('./ui/views/home/HomepageView'))

const AsyncCrew = lazy(() => import('./ui/views/crew/CrewView'))

const AsyncAbout = lazy(() => import('./ui/views/about/AboutView'))

const AsyncLogin = lazy(() => import('./ui/views/login/LoginView'))

const AsyncApplication = lazy(() => import('./ui/views/application/ApplicationView'))

const AsyncForgetPwd = lazy(() => import('./ui/views/forget-pwd/ForgetPwdView'))

const AsyncUserAchievement = lazy(() => import('./ui/views/user-achievement/UserAchievementView'))

const BasicRoute = (): JSX.Element => {
	const mask = <Backdrop style={{zIndex: 20000, color: 'white'}} open={true}>
		<CircularProgress style={{zIndex: 20000}} color='inherit'/>
	</Backdrop>
	return <>
		<Suspense fallback={mask}>
			<Routes>
				<Route path='/' element={<AsyncHome />}/>
				<Route path='crew/*' element={<AsyncCrew />}/>
				<Route path='/about' element={<AsyncAbout />}/>
				<Route path='/login' element={<AsyncLogin />}/>
				<Route path='/forget_pwd' element={<AsyncForgetPwd />}/>
				<Route path='/application' element={<AsyncApplication />}/>
				<Route path='/achievement' element={<AsyncUserAchievement />}/>
			</Routes>
		</Suspense>
	</>
}

export default BasicRoute
