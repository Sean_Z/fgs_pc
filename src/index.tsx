import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import {Provider} from 'react-redux'
import Routers from './router'
import store, {persist, useAppSelector} from './redux/store'
import {PersistGate} from 'redux-persist/lib/integration/react'
import {BrowserRouter} from 'react-router-dom'
import {createBrowserHistory} from 'history'
import Backdrop from '@material-ui/core/Backdrop'
import CircularProgress from '@material-ui/core/CircularProgress'
import Snackbar from '@material-ui/core/Snackbar'
import {Alert} from '@material-ui/lab'
import {MuiThemeProvider} from '@material-ui/core'
import {theme} from './ui/theme/Theme'

const history = createBrowserHistory()
const UI = (): JSX.Element => {
	return <BrowserRouter>
		<Backdrop style={{zIndex: 20000, color: 'white'}} open={useAppSelector((state) => state.uiReducer.isLoading)}>
			<CircularProgress style={{zIndex: 20000}} color='inherit'/>
		</Backdrop>
		<Snackbar style={{zIndex: 20000}} open={useAppSelector((state) => state.uiReducer.showSnackMessage.open)} autoHideDuration={1000}>
			<Alert severity={useAppSelector((state) => state.uiReducer.showSnackMessage.messageType)}>
				{useAppSelector((state) => state.uiReducer.showSnackMessage.content)}
			</Alert>
		</Snackbar>
		<MuiThemeProvider theme={theme}>
			<Routers/>
		</MuiThemeProvider>
	</BrowserRouter>
}

ReactDOM.render(
	<PersistGate loading={null} persistor={persist} key={history.location.key}>
		<Provider store={store}>
			<UI/>
		</Provider>
	</PersistGate>,
	document.getElementById('root')
)