import {JSEncrypt} from 'jsencrypt'

export default class SecurityUtil {

	public static rsaEncrypt(params: string): {data: string} {
		const encryptor = new JSEncrypt({log: true})
		const pubKey = '-----BEGIN PUBLIC KEY-----' +
            'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwXQixS9OuejFuMxRQQza' +
            'kLbbL7hABK+il3oU11Z2UR2JUUSj+UfkPEofPbTYERS+oFQzdtaPDwcQNsG1lCKs' +
            '8CtzlJ5qsRtBR/NloFo7YoxYcH1e3qBC8msS7MduNtbezLZS45Y7C5Fv6FPzCwHb' +
            'J1f3Fw3J+A1JasHCXUzgIVyP0rwAd+fywvdzYmN87UvGd+9U31Cf76129UMKlrJa' +
            'i3PLI2c+F225Z3sfB6Fw+s4G/VeMWsJUb/TxI+9T0KqKO35Bt+wuTDUjVTO4TOif' +
            'clkdk8asAjmdoUQnUCW9I8zwRlOJYhPj7jCf/mnPSzJGxfSmDb/CLqDLczq1rzBT' +
            'OwIDAQAB' +
            '-----END PUBLIC KEY-----'
		encryptor.setPublicKey(pubKey)
		return {data: encryptor.encrypt(params) as string}
	}

	public static rsaDecrypt(encryptedData: string): string | false {
		const encryptor = new JSEncrypt({log: true})
		const pubKey = '-----BEGIN PUBLIC KEY-----' +
            'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwXQixS9OuejFuMxRQQza' +
            'kLbbL7hABK+il3oU11Z2UR2JUUSj+UfkPEofPbTYERS+oFQzdtaPDwcQNsG1lCKs' +
            '8CtzlJ5qsRtBR/NloFo7YoxYcH1e3qBC8msS7MduNtbezLZS45Y7C5Fv6FPzCwHb' +
            'J1f3Fw3J+A1JasHCXUzgIVyP0rwAd+fywvdzYmN87UvGd+9U31Cf76129UMKlrJa' +
            'i3PLI2c+F225Z3sfB6Fw+s4G/VeMWsJUb/TxI+9T0KqKO35Bt+wuTDUjVTO4TOif' +
            'clkdk8asAjmdoUQnUCW9I8zwRlOJYhPj7jCf/mnPSzJGxfSmDb/CLqDLczq1rzBT' +
            'OwIDAQAB' +
            '-----END PUBLIC KEY-----'
		encryptor.setPublicKey(pubKey)
		return encryptor.decrypt(encryptedData)
	}
}
