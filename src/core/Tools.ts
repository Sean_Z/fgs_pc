/**
 * @method 此文件为公共方法文件
 * @author Sean_Zhang
 */
import {UIDispatcher} from '@store/action/UIDispatcher'
import airline from './airline.json'
import {Links} from '@assets/Constant'
import infinitePng from '../assets/img/icon/infinite.png'
import ufoPng from '../assets/img/icon/ufo.png'
import * as echarts from 'echarts'
import {EChartsOption} from 'echarts'
import {LatLngExpression, Polyline} from 'leaflet'
import * as turf from '@turf/turf'
import _ from 'lodash'

export class Tools {

    /**
     * @author Sean_Zhang
     * @date 2021/2/27 7:55 pm
     * @description 获取链接参数
     * @param arg
     * @return 参数
     */
    public static getParam(arg: string): string {
        const result: RegExpMatchArray | null = window.location.search.match(new RegExp(`[?&]${arg}=([^&]+)`, 'i'))
        if (result == null || result.length < 1) {
            return ''
        }
        return result[1]
    }

    public static handleException(msg: string, withException: boolean = true): void {
        UIDispatcher.setSnackMessage({messageType: 'error', content: msg, open: true})
        if (withException) throw Error(msg)
    }

    public static getAirlineIcon(airlineName: string): string {
        const temp = Array.from(airline).filter((obj) => obj.airline_name === airlineName)
        if (temp.length !== 0) {
            return `${Links.airline_icon + temp[0].icao_code}_logo0.png`
        } else if (airlineName.includes('Infinite') || airlineName.includes('Generic')) {
            return infinitePng
        } else {
            return ufoPng
        }
    }

    public static buildEcharts(containerId: string, config: EChartsOption): void {
        const chartDom = document.getElementById(containerId)
        const myChart = echarts.init(chartDom!)
        config && myChart.setOption(config)
    }
}

export class CalculateMethod {
    /**
     * @author Sean_Zhang
     * @date 2021/4/7 8:07 pm
     * @description 转换UTC时间
     * @param date 时间戳
     * @return 当地时间 小时和分钟
     */
    public static convertUTCDateToLocalDate(date: Date): { hour: number, minute: number } {

        const newDate = new Date(date.getTime() + date.getTimezoneOffset() * 60 * 1000)
        const offset = date.getTimezoneOffset() / 60
        const hours = date.getHours()
        newDate.setHours(hours - offset)
        return {
            hour: newDate.getHours(),
            minute: newDate.getMinutes()
        }
    }

    public static calculateRemainRange(wholeRangePosition: AirwayPointList, airwayLineList: AirwayPointList): number {
        const wholeRange = this.entireDistance(wholeRangePosition)
        const flewRange = this.entireDistance(airwayLineList)
        return wholeRange - flewRange
    }

    public static calculateRemainMinutes(remainRange: number, speed: number): number {
        const remainTime = remainRange / speed
        return remainTime * 60
    }

    public static entireDistance(pointList: string | any[]): number {
        const temp = []
        let count = 0
        for (let i = 0; i < pointList.length - 2; i++) {
            temp.push({point1: pointList[i], point2: pointList[i + 2]})
            count = count + this.getDistance(temp[i].point1, temp[i].point2)
        }
        return count / 2 / 1.852
    }

    private static getDistance(point1: { [x: string]: number }, point2: { [x: string]: number }): number {
        const radLat1 = point1.latitude * Math.PI / 180.0
        const radLat2 = point2.latitude * Math.PI / 180.0
        const a = radLat1 - radLat2
        const b = point1.longitude * Math.PI / 180.0 - point2.longitude * Math.PI / 180.0
        let s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) +
            Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2)))
        // EARTH_RADIUS
        s = s * 6371.137
        s = Math.round(s * 10000) / 10000
        return s
    }

    public static turfPolylineAndMark(lastPoint: LatLngExpression, airwayLineLayer: Polyline): LatLngExpression {
        const lastPointCopy: number[] = _.cloneDeep<number[]>(lastPoint as number[])
        if (lastPointCopy.length < 2) return lastPoint
        const pt = turf.point([lastPointCopy[1], lastPointCopy[0]])
        const snapped = turf.nearestPointOnLine(airwayLineLayer.toGeoJSON(), pt, {units: 'nauticalmiles'})
        const center = snapped.geometry.coordinates
        return center.includes(Infinity) ? lastPoint : [center[1], center[0]]
    }
}

type AirwayPointList = { latitude: number, longitude: number }[]