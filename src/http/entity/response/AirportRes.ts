export interface AirportRes {
	readonly result: AirportModal[]
}

export interface AirportModal {
	readonly continent: string;
	readonly elevationFt: number;
	readonly gpsCode: string;
	readonly iataCode: string;
	readonly id: number;
	readonly ident: string;
	readonly isoCountry: string;
	readonly isoRegion: string;
	readonly latitudeDeg: number;
	readonly longitudeDeg: number;
	readonly municipality: string;
	readonly name: string;
	readonly scheduledService: string;
	readonly type: string;
	readonly wikipediaLink: string;
}