export interface InfiniteFlightUser {
    onlineFlights: number
    violations: number
    xp: number
    landingCount: number
    flightTime: number
    atcOperations: number
    atcRank: number
    grade: number
    hash: string
    violationCountByLevel: {
        level1: number
        level2: number
        level3: number
    },
    roles: number[],
    userId: string
    virtualOrganization: string
    discourseUsername: string
    groups: string[],
    errorCode: number
}