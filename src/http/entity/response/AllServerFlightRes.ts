/**
 * @author Sean_Zhang
 * @date 2021/4/25 12:19 pm
 * @description fetchFlight 获取所有航班基础数控List
 */
export interface AllServerFlightRes {
	data: FlightDetail[],
}

export interface FlightDetail {
	position: {
		latitude: number,
		longitude: number
	},
	angle: number,
	liveryId: string,
	flightId: string,
	aircraftId: string,
	altitude: number,
	callSign: string,
	displayName: string,
	speed: number,
	verticalSpeed: number,
	livery: [
		{
			readonly id: string
			readonly aircraftID: string
			readonly aircraftName: string
			readonly liveryName: string
		}
	]
}