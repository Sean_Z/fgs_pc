export interface MemberListRes {
    readonly endRow: number
    readonly hasNextPage: boolean
    readonly hasPreviousPage: boolean
    readonly isFirstPage: boolean
    readonly isLastPage: boolean
    readonly list: MemberListIFSList[]
    readonly navigateFirstPage: number
    readonly navigateLastPage: number
    readonly navigatePages: number
    readonly navigatepageNums: number[]
    readonly nextPage: number
    readonly pageNum: number
    readonly pageSize: number
    readonly pages: number
    readonly prePage: number
    readonly size: number
    readonly startRow: number
    readonly total: number
}

export interface MemberListIFSList {
    readonly userId: string,
    readonly username: string,
    readonly infiniteFlightUserName: string,
    readonly mail: string,
    readonly pwd: string,
    readonly country: string,
    readonly vaild: number,
    readonly createTime: Date,
    readonly updateTime: Date,
    readonly position: string,
}