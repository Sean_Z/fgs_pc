/**
 * @author Sean_Zhang
 * @date 2021/2/25 3:22 pm
 * @description 当期航班的航线信息
 * @param latitude 当前航班此刻当前航点报告时维度
 * @param longitude 当前航班此刻当前航点报告时经度
 * @param altitude 当前航班此刻当前航点报告时高度
 * @param date 当前航班此刻当前航点报告时时间 yyyy-MM-dd HH:mm:ss z
 */
export interface AirwayLineRes {
	readonly latitude: number,
	readonly longitude: number,
	readonly altitude: number,
	readonly time: string,
	readonly groundSpeed: number
}