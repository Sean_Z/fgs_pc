/**
 * make flight plan
 */
export interface FlightPlanRes extends ResponseIFS {
	readonly routeInfo: string,
	readonly distance: number,
	readonly coordinateList: LatLngGroup,
	readonly departureName: string,
	readonly arrivalName: string
}

export interface ResponseIFS {
	readonly message: string,
	readonly status: number,
}

export interface LatLng {
	latitude: number
	longitude: number
}
export type LatLngGroup = LatLng[]