export interface UserGradeRes {
	total12MonthsViolations: number;
	gradeDetails: UserGradeResGradeDetails;
	totalXP: number;
	atcOperations: number;
	atcRank: number;
	lastLevel1ViolationDate: string;
	lastLevel2ViolationDate: string;
	lastLevel3ViolationDate: string;
	lastReportViolationDate: string;
	violationCountByLevel: UserGradeResViolationCountByLevel;
	roles: number[];
	userId: string;
	virtualOrganization: string;
	discourseUsername: string;
	groups: any[];
	errorCode: number;
}
export interface UserGradeResGradeDetailsGradesRulesDefinition {
	name: string;
	description: string;
	property: string;
	operator: number;
	period: number;
	order: number;
	group: number;
}
export interface UserGradeResGradeDetailsGradesRules {
	ruleIndex: number;
	referenceValue: number;
	userValue: number;
	state: number;
	userValueString: string;
	referenceValueString: string;
	definition: UserGradeResGradeDetailsGradesRulesDefinition;
}
export interface UserGradeResGradeDetailsGrades {
	rules: UserGradeResGradeDetailsGradesRules[];
	index: number;
	name: string;
	state: number;
}
export interface UserGradeResGradeDetailsRuleDefinitions {
	name: string;
	description: string;
	property: string;
	operator: number;
	period: number;
	order: number;
	group: number;
}
export interface UserGradeResGradeDetails {
	grades: UserGradeResGradeDetailsGrades[];
	gradeIndex: number;
	ruleDefinitions: UserGradeResGradeDetailsRuleDefinitions[];
}
export interface UserGradeResViolationCountByLevel {
	level1: number;
	level2: number;
	level3: number;
}