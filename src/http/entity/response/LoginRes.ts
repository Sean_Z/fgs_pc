export interface LoginRes {
	status: number;
	message: string;
	nodeToken: string;
	accessToken: string;
	refreshToken: string;
	position: string;
	username: string;
	email: string;
	currentPosition: string;
	infiniteFlightUserId: string
	infiniteFlightUserName: string
}