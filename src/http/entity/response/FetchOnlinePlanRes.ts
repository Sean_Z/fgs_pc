/**
 * @author Sean_Zhang
 * @date 2021/2/25 3:16 pm
 * @description 当前航班的基础信息
 * @param routeInfo 一个以空格分割航路点名称的字符串 表示航路
 * @param departure 当前航班起飞机场ICAO
 * @param arrival 当前航班到达机场ICAO
 * @param latitude 维度
 * @param longitude 经度
 */
export interface FetchOnlinePlanRes {
	readonly status: number,
	readonly routeInfo: string,
	readonly departure: string,
	readonly arrival: string,
	readonly wholeRangePosition: {
		readonly latitude: number,
		readonly longitude: number
	}[]
}

export interface LatLng {
	readonly latitude: number,
	readonly longitude: number
}