export interface InfiniteFlightLogsRes {
    readonly pageIndex: number
    readonly totalPages: number
    readonly totalCount: number
    readonly hasPreviousPage: boolean
    readonly hasNextPage: boolean
    readonly data: InfiniteFlightLog[]
}

export interface InfiniteFlightLog {
    readonly id: string
    readonly created: string
    readonly userId: string
    readonly aircraftId: string
    readonly destName: string
    readonly deptName: string
    readonly liveryId: string
    readonly callsign: string
    readonly server: string
    readonly dayTime: number
    readonly nightTime: number
    readonly totalTime: number
    readonly landingCount: number
    readonly originAirport: string
    readonly destinationAirport: string
    readonly xp: number
}