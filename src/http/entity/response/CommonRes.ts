export interface CommonRes {
    readonly status: number,
    readonly message: string
}