export interface LogFlightReq {
    readonly rate: number
    readonly delay: number
    readonly searchFavorite: number
}