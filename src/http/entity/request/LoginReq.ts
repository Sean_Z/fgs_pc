export interface LoginReq {
    username: string,
    password: string,
    readonly type: number
}