/**
 * 获取航路计划RequestBean
 * @param fromICAO {string} 起飞机场ICAO
 * @param toICAO {string} 到达机场ICAO
 */
export interface GetFlightPlanRequest {
    readonly fromICAO: string,
    readonly toICAO: string
}

/**
 * 查询IF用户飞行日志
 * userId 用户名
 * page 查询页码
 */
export interface InfiniteFlightLogsReq {
    readonly userName: string
    page?: number
}