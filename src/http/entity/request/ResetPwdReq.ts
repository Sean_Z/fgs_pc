/**
 * 校验短信验证码RequestBean
 * @param email {string} 邮件地址
 * @param verifyCode {string} 验证码
 * @param resetPwd {string} 重置密码
 */
export interface ResetPwdReq {
    readonly email: string,
    readonly verifyCode: string,
    readonly resetPwd: string
}