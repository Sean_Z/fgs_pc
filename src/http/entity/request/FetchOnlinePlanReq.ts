export interface FetchOnlinePlanReq {
    readonly serverType: number
    readonly flightId: string
    readonly aircraftId: string
}