export interface SubmitApplicationReq {
    readonly age: string,
    readonly country: string,
    readonly violations: number,
    readonly ifFlightTime: number,
    readonly landings: number,
    readonly username: string,
    readonly email: string,
    readonly joinedGroup: string
}