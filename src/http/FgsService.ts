import {UserGradeRes} from '@http/entity/response/UserGradeRes'
import {LoginRes} from '@http/entity/response/LoginRes'
import {FlightPlanRes} from '@http/entity/response/FlightPlanRes'
import {AllServerFlightRes} from '@http/entity/response/AllServerFlightRes'
import {GetFlightPlanRequest, InfiniteFlightLogsReq} from './/entity/request/InfiniteFlightLogReq'
import {InfiniteFlightLogsRes} from '@http/entity/response/InfiniteFlightLogsRes'
import {AirwayLineRes} from '@http/entity/response/AirwayLineRes'
import {AirportRes} from '@http/entity/response/AirportRes'
import {CommonRes} from '@http/entity/response/CommonRes'
import {FetchOnlinePlanRes} from '@http/entity/response/FetchOnlinePlanRes'
import {FetchOnlinePlanReq} from '@http/entity/request/FetchOnlinePlanReq'
import {SubmitApplicationReq} from '@http/entity/request/SubmitApplicationReq'
import {MemberListRes} from '@http/entity/response/MemberListRes'
import {ResetPwdReq} from '@http/entity/request/ResetPwdReq'
import HttpExecutor from './HttpExecutor'
import {InfiniteFlightUser} from '@http/entity/response/InfiniteFlightUserRes'
import {LoginReq} from '@http/entity/request/LoginReq'
import {LogFlightReq} from '@http/entity/request/LogFlightReq'

export class FgsService {
    public static async login(request: LoginReq): Promise<LoginRes> {
        return await HttpExecutor.post('/user/login', request)
    }

    public static async fetchFlightPlan(request: GetFlightPlanRequest): Promise<FlightPlanRes> {
        return await HttpExecutor.post('/common/getPlan', request)
    }

    /**
     * 获取全服飞机
     * @param serverType 指定服务器
     */
    public static async fetchFlight(serverType: number): Promise<AllServerFlightRes> {
        return await HttpExecutor.get<AllServerFlightRes>(`/infiniteFlight/fetchFlights?serverType=${serverType}`)
    }

    /**
     * 查询记录在infinite flight数据库的航班日志
     * @param data {InfiniteFlightLogsReq}
     */
    public static async infiniteFlightLogs(data: InfiniteFlightLogsReq): Promise<InfiniteFlightLogsRes> {
        return await HttpExecutor.post<InfiniteFlightLogsRes>('/infiniteFlight/infiniteFlightLogs', data)
    }

    /**
     * 查询指定航班的航路计划
     */
    public static async fetchOnlinePlan(request: FetchOnlinePlanReq): Promise<FetchOnlinePlanRes> {
        return await HttpExecutor.post<FetchOnlinePlanRes>('/infiniteFlight/fetchOnlinePlan', request)
    }

    /**
     * 获取飞行航线
     * @param flightId {string} 当前查询的航班id
     */
    public static async getAirwayLine(flightId: string): Promise<AirwayLineRes[]> {
        return await HttpExecutor.post<AirwayLineRes[]>('/infiniteFlight/airwayLine', {flightId})
    }

    /**
     * 获取FGS 成员列表信息
     */
    public static async getFgsUserList(): Promise<MemberListRes> {
        return await HttpExecutor.post<MemberListRes>('/user/getUserInfo', {isUserList: 0})
    }

    /**
     * 获取Infinite Flight 用户信息
     * @param username {string} 用户名
     */
    public static async getUserInfo(username: string): Promise<InfiniteFlightUser> {
        const userDisplayName = {displayName: username}
        return await HttpExecutor.post<InfiniteFlightUser>('/infiniteFlight/userInfo', userDisplayName)
    }

    /**
     * 发送邮件验证码
     * @param email {string} 邮箱号
     */
    public static async sendVerifyCode(email: string): Promise<CommonRes> {
        return await HttpExecutor.post<CommonRes>('/user/sendVerifyCode', {email})
    }

    /**
     * 重置密码
     */
    public static async resetPwd(request: ResetPwdReq): Promise<CommonRes> {
        return await HttpExecutor.post('/user/verifyCode', request)
    }

    /**
     * github登录
     * @param code {string} 授权码
     */
    public static async githubLogin(code: string): Promise<LoginRes> {
        return await HttpExecutor.post<LoginRes>('/user/githubLogin', {code})
    }

    /**
     * 提交申请表单
     */
    public static async submitApplication(submitData: SubmitApplicationReq): Promise<CommonRes> {
        return await HttpExecutor.post<CommonRes>('/user/application', submitData)
    }

    public static async searchAirport(icaoCode: string): Promise<AirportRes> {
        return await HttpExecutor.post('/fms/searchAirport', {icaoCode})
    }

    public static async logFlight(log: LogFlightReq): Promise<CommonRes> {
        return await HttpExecutor.post('/fms/logFlight', log)
    }

    public static async getUserGrade(userId: string): Promise<UserGradeRes> {
        return await HttpExecutor.post('/infiniteFlight/userGrade', {userId})
    }
}