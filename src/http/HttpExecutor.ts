import {HttpConfig} from './config/HttpConfig'
import axios, {AxiosRequestConfig, AxiosResponse} from 'axios'
import {createBrowserHistory} from 'history'
import store from '@store/store'
import SecurityUtil from '@core/SecurityUtil'

export default class HttpExecutor {
	private readonly history = createBrowserHistory()
	private static readonly CancelToken = axios.CancelToken
	private static readonly source = this.CancelToken.source()
	private readonly checkTokenWhiteList = (url: string): boolean => HttpConfig.tokenWhiteList.includes(url)
	private static readonly shouldEncrypt = (url: string): boolean => HttpConfig.encryptList.includes(url)
	private readonly axiosInstance = axios.create()

	constructor() {
		this.axiosInstance.defaults.baseURL = import.meta.env.VITE_REQUEST_BASE_URL
		this.axiosInstance.interceptors.request.use(
			(config: AxiosRequestConfig) => this.interceptRequest(config),
			(err) => Promise.reject(err)
		)
		this.axiosInstance.interceptors.response.use(
			(res: AxiosResponse) => this.interceptResponse(res),
			(error) => Promise.reject(error)
		)
	}

	/**
	 * axios request 拦截器
	 * @param config
	 * @private
	 */
	private interceptRequest(config: AxiosRequestConfig): AxiosRequestConfig {
		if (localStorage.nodeToken && localStorage.accessToken && localStorage.refreshToken) {
			if (!config.headers) config.headers = {}
			config.headers.nodeToken = localStorage.nodeToken
			config.headers.accessToken = localStorage.accessToken
			config.headers.refreshToken = localStorage.refreshToken
			return config
		} else if (config.url === '/login' || this.checkTokenWhiteList(this.history.location.pathname)) {
			return config
		} else {
			localStorage.removeItem('persist:root')
			window.location.replace('/login')
			HttpExecutor.cancelRequest('should login')
			return config
		}
	}

	/**
	 * axios response拦截器
	 * @param response AxiosResponse
	 */
	private readonly interceptResponse = (response: AxiosResponse): unknown => {
		if (response.headers.signVerify === 'SignVerify failed!') {
			return {status: 4, message: 'sign verify failed'}
		}
		if (response.data.accessToken) {
			localStorage.setItem('accessToken', response.data.accessToken)
			localStorage.setItem('refreshToken', response.data.refreshToken)
			localStorage.setItem('nodeToken', response.data.nodeToken)
		}
		const tokenErrorRuleMap = [
			response.data.message === 'jwt must be provided',
			JSON.stringify(response).includes('JsonWebTokenError'),
			response.data.message === 'token已失效，请重新登录'
		]
		if (tokenErrorRuleMap.includes(true)) {
			localStorage.removeItem('persist:root')
			window.location.replace('/login')
		} else {
			if (response.data.isTokenRefreshed) {
				localStorage.setItem('accessToken', response.data.accessToken)
				localStorage.setItem('refreshToken', response.data.refreshToken)
				localStorage.setItem('nodeToken', response.data.nodeToken)
			} else if (response.data.tokenError) {
				localStorage.removeItem('persist:root')
				window.location.replace('/login')
			}
			if (response.data.status === 1) {
				HttpExecutor.handleException(response.data.message)
			}
			return response.data
		}
	}

	/**
	 * get请求
	 * @author Sean_Zhang
	 * @method get
	 * @param url 请求地址
	 */
	public static readonly get = async <T>(url: string): Promise<T> => {
		const instance = new HttpExecutor()
		const config = {cancelToken: this.source.token}
		const response = await instance.axiosInstance.get(url, config).catch((err) => console.log(err))
		store.dispatch({type: 'uiReducer/setIsLoading', data: false})
		return response as unknown as T
	}

	/**
	 * post请求
	 * @method post
	 * @author Sean_Zhang
	 * @param url 请求地址
	 * @param params 请求参数 可选
	 */
	public static readonly post = async <T>(url: string, params?: Record<string, any>): Promise<T> => {
		const instance = new HttpExecutor()
		const config = {cancelToken: this.source.token}
		const requestBody = this.shouldEncrypt(url) ? SecurityUtil.rsaEncrypt(JSON.stringify(params)) : params
		const response = await instance.axiosInstance.post(url, requestBody, config)
			.catch(() => this.handleException())
		return response as unknown as T
	}

	private static handleException(errorMsg = '服务异常'): void {
		store.dispatch({type: 'uiReducer/setIsLoading', payload: false})
		store.dispatch({type: 'uiReducer/setSnackBarMessage', data: {open: true, messageType: 'error', content: errorMsg}})
		setTimeout(() => {
			store.dispatch({type: 'uiReducer/setSnackBarMessage', data: {open: false, messageType: 'error', content: errorMsg}})
		}, 2000)
		throw Error(errorMsg)
	}

	/**
	 * 取消请求
	 * @param msg 取消原因
	 */
	public static readonly cancelRequest = (msg: string): void => this.source.cancel(msg)
}
