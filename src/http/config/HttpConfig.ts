
export class HttpConfig {
	public static readonly tokenWhiteList = ['/verifyCode', '/application', '/forget_pwd', '/githubLogin', '/login']
	public static readonly encryptList = ['/infiniteFlight/fetchOnlinePlan', '/infiniteFlight/airwayLine', '/infiniteFlight/getAllServerFlight?shouldUseSimpleResult=false']
}