import {defineConfig} from 'vite'
import react from '@vitejs/plugin-react'
import * as path from 'path'
import viteCompression from 'vite-plugin-compression'

export default defineConfig({
    resolve: {
        alias: {
            '@http': path.resolve(__dirname, 'src/http'),
            '@assets': path.resolve(__dirname, 'src/assets'),
            '@css': path.resolve(__dirname, 'src/assets/css'),
            '@core': path.resolve(__dirname, 'src/core'),
            '@store': path.resolve(__dirname, 'src/redux'),
        }
    },
    plugins: [
        viteCompression({threshold: 1000 * 300}),
        react(
        {
            babel: {
                plugins: [
                    ['@babel/plugin-proposal-decorators', {legacy: true}],
                    ['@babel/plugin-proposal-class-properties', {loose: true}],
                ],
            },
        }
    )]
})
